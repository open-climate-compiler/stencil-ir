//===- ConvertIIRToStencil.cpp - IIR to Stencil dialect conversion --------===//
//
// Copyright 2019 Jean-Michel Gorius
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// =============================================================================
//
// This file implements a pass to convert the MLIR IIR dialect into the Stencil
// dialect.
//
//===----------------------------------------------------------------------===//

#include "mlir/Conversion/StencilToAffine/ConvertStencilToAffine.h"
#include "mlir/Dialect/AffineOps/AffineOps.h"
#include "mlir/Dialect/StandardOps/Ops.h"
#include "mlir/Dialect/Stencil/Stencil/StencilOps.h"
#include "mlir/IR/AffineMap.h"
#include "mlir/IR/Module.h"
#include "mlir/IR/StandardTypes.h"

#include "llvm/Support/raw_ostream.h"

using namespace mlir;

//===----------------------------------------------------------------------===//
// Common
//===----------------------------------------------------------------------===//

namespace {

// Compute offset = isize * jsize * k + isize * (j + boundarySize) + (i +
// boundarySize)
Value *computeOffset(Value *i, Value *j, Value *k, Value *isize, Value *jsize,
                     Value *ksize, Value *boundarySize,
                     PatternRewriter &rewriter, Location loc) {
  auto kOffsetLhs = rewriter.create<MulIOp>(loc, jsize, isize).getResult();
  auto kOffset = rewriter.create<MulIOp>(loc, kOffsetLhs, k).getResult();
  auto jj = rewriter.create<AddIOp>(loc, j, boundarySize).getResult();
  auto jOffset = rewriter.create<MulIOp>(loc, isize, jj).getResult();
  auto offsetLhs = rewriter.create<AddIOp>(loc, kOffset, jOffset).getResult();
  auto ii = rewriter.create<AddIOp>(loc, i, boundarySize).getResult();
  auto offset = rewriter.create<AddIOp>(loc, offsetLhs, ii).getResult();

  return offset;
}

Value *getRealKValue(Value *kVar) {
  Value *realKValue = nullptr;
  for (auto &use : kVar->getUses()) {
    if (isa<AffineApplyOp>(use.getOwner())) {
      realKValue = use.getOwner()->getResult(0);
      break;
    }
  }
  return realKValue;
}

} // namespace

//===----------------------------------------------------------------------===//
// Operation conversion
//===----------------------------------------------------------------------===//

namespace {

class StencilEndOpConversion final : public ConversionPattern {
public:
  explicit StencilEndOpConversion(MLIRContext *context)
      : ConversionPattern(stencil::StencilEndOp::getOperationName(), 1,
                          context) {}

  PatternMatchResult
  matchAndRewrite(Operation *operation, ArrayRef<Value *> operands,
                  ConversionPatternRewriter &rewriter) const override {
    rewriter.replaceOpWithNewOp<ReturnOp>(operation);

    return matchSuccess();
  }
};

class StencilOpConversion final : public ConversionPattern {
public:
  explicit StencilOpConversion(MLIRContext *context)
      : ConversionPattern(stencil::StencilOp::getOperationName(), 1, context) {}

  PatternMatchResult
  matchAndRewrite(Operation *operation, ArrayRef<Value *> operands,
                  ConversionPatternRewriter &rewriter) const override {
    auto stencilOp = cast<stencil::StencilOp>(operation);
    auto stencilName =
        operation->getAttr("stencilName").cast<StringAttr>().getValue();

    auto memRefType = MemRefType::get(-1, rewriter.getF64Type());
    SmallVector<Type, 3> parameterTypes(stencilOp.getBody().getNumArguments(),
                                        memRefType);

    // Add domain size and boundary size parameters.
    // These appear in the following order:
    // (isize, jsize, ksize, boundary_size)
    int numAdditionalParameters = 4;
    parameterTypes.insert(std::begin(parameterTypes), numAdditionalParameters,
                          rewriter.getIndexType());
    auto funcOp = rewriter.create<FuncOp>(
        operation->getLoc(), stencilName,
        rewriter.getFunctionType(parameterTypes, llvm::None), llvm::None);
    auto entryBlock = funcOp.addEntryBlock();
    for (unsigned i = 0, e = stencilOp.getBody().getNumArguments(); i < e;
         ++i) {
      stencilOp.getBody().getArgument(i)->replaceAllUsesWith(
          entryBlock->getArgument(i + numAdditionalParameters));
    }
    auto &stencilOperations = stencilOp.getBody().getOperations();
    entryBlock->getOperations().splice(entryBlock->begin(), stencilOperations);

    rewriter.eraseOp(operation);

    return matchSuccess();
  }
};

class IIROpConversion final : public ConversionPattern {
public:
  explicit IIROpConversion(MLIRContext *context)
      : ConversionPattern(stencil::IIROp::getOperationName(), 1, context) {}

  PatternMatchResult
  matchAndRewrite(Operation *operation, ArrayRef<Value *> operands,
                  ConversionPatternRewriter &rewriter) const override {
    auto iirOp = cast<stencil::IIROp>(operation);
    auto &ops = operation->getBlock()->getOperations();
    auto &bodyOps = iirOp.getBody().getOperations();
    ops.splice(ops.begin(), bodyOps, std::begin(bodyOps),
               std::prev(std::end(bodyOps)));
    rewriter.eraseOp(operation);

    return matchSuccess();
  }
};

class FieldOpConversion final : public ConversionPattern {
public:
  explicit FieldOpConversion(MLIRContext *context)
      : ConversionPattern(stencil::FieldOp::getOperationName(), 1, context) {}

  PatternMatchResult
  matchAndRewrite(Operation *operation, ArrayRef<Value *> operands,
                  ConversionPatternRewriter &rewriter) const override {
    auto loc = operation->getLoc();

    auto stencilFunc = operation->getParentOfType<FuncOp>();
    if (!stencilFunc) {
      operation->emitError("expected parent function operation");
      return matchFailure();
    }

    auto isize = stencilFunc.getArgument(0);
    auto jsize = stencilFunc.getArgument(1);
    auto ksize = stencilFunc.getArgument(2);
    auto ijsize = rewriter.create<MulIOp>(loc, isize, jsize).getResult();
    auto size = rewriter.create<MulIOp>(loc, ijsize, ksize).getResult();

    rewriter.replaceOpWithNewOp<AllocOp>(
        operation,
        MemRefType::get(-1, rewriter.getF64Type(),
                        rewriter.getMultiDimIdentityMap(1), 0),
        size);

    return matchSuccess();
  }
};

class VarOpConversion final : public ConversionPattern {
public:
  explicit VarOpConversion(MLIRContext *context)
      : ConversionPattern(stencil::VarOp::getOperationName(), 1, context) {}

  PatternMatchResult
  matchAndRewrite(Operation *operation, ArrayRef<Value *> operands,
                  ConversionPatternRewriter &rewriter) const override {
    rewriter.replaceOpWithNewOp<AllocOp>(
        operation, MemRefType::get(llvm::None, rewriter.getF64Type()));

    return matchSuccess();
  }
};

class FieldAccessOpConversion final : public ConversionPattern {
public:
  explicit FieldAccessOpConversion(MLIRContext *context)
      : ConversionPattern(stencil::FieldAccessOp::getOperationName(), 1,
                          context) {}

  PatternMatchResult
  matchAndRewrite(Operation *operation, ArrayRef<Value *> operands,
                  ConversionPatternRewriter &rewriter) const override {
    auto fieldAccessOp = cast<stencil::FieldAccessOp>(operation);

    // TODO: Skip if the value of the pointer is never dereferenced

    auto offset = fieldAccessOp.offset().getValue();
    if (offset.size() != 3) {
      operation->emitError("expected offset to have three components");
      return matchFailure();
    }

    int64_t ioffsetAttr = offset[0].cast<IntegerAttr>().getInt();
    int64_t joffsetAttr = offset[1].cast<IntegerAttr>().getInt();
    int64_t koffsetAttr = offset[2].cast<IntegerAttr>().getInt();

    auto loc = operation->getLoc();

    auto ioffset =
        rewriter.create<ConstantIndexOp>(loc, ioffsetAttr).getResult();
    auto joffset =
        rewriter.create<ConstantIndexOp>(loc, joffsetAttr).getResult();
    auto koffset =
        rewriter.create<ConstantIndexOp>(loc, koffsetAttr).getResult();

    // Update these according to the generated loop order
    // Assume JIK order here
    auto kLoop = operation->getParentOfType<AffineForOp>();
    if (!kLoop) {
      operation->emitError("expected field access to be enclosed in `k` loop");
      return matchFailure();
    }
    auto iLoop = kLoop.getParentOfType<AffineForOp>();
    if (!iLoop) {
      operation->emitError("expected `k` loop to be enclosed in `i` loop");
      return matchFailure();
    }
    auto jLoop = iLoop.getParentOfType<AffineForOp>();
    if (!jLoop) {
      operation->emitError("expected `i` loop to be enclosed in `j` loop");
      return matchFailure();
    }

    auto i = rewriter.create<AddIOp>(loc, iLoop.getInductionVar(), ioffset);
    auto j = rewriter.create<AddIOp>(loc, jLoop.getInductionVar(), joffset);
    auto kVar = kLoop.getInductionVar();
    Value *realKValue = getRealKValue(kVar);
    if (!realKValue) {
      operation->emitError(
          "expected 'k' index to be computed by an affine apply operation");
      return matchFailure();
    }
    auto k = rewriter.create<AddIOp>(loc, realKValue, koffset);

    auto stencilFunc = operation->getParentOfType<FuncOp>();
    if (!stencilFunc) {
      operation->emitError("expected parent function operation");
      return matchFailure();
    }
    auto isize = stencilFunc.getArgument(0);
    auto jsize = stencilFunc.getArgument(1);
    auto ksize = stencilFunc.getArgument(2);
    auto boundarySize = stencilFunc.getArgument(3);

    auto arrayOffset = computeOffset(i, j, k, isize, jsize, ksize, boundarySize,
                                     rewriter, loc);

    rewriter.replaceOpWithNewOp<LoadOp>(operation, operands[0],
                                        llvm::makeArrayRef({arrayOffset}));
    return matchSuccess();
  }
};

class VarAccessOpConversion final : public ConversionPattern {
public:
  explicit VarAccessOpConversion(MLIRContext *context)
      : ConversionPattern(stencil::VarAccessOp::getOperationName(), 1,
                          context) {}

  PatternMatchResult
  matchAndRewrite(Operation *operation, ArrayRef<Value *> operands,
                  ConversionPatternRewriter &rewriter) const override {
    rewriter.replaceOpWithNewOp<LoadOp>(operation, operands[0]);

    return matchSuccess();
  }
};

class WriteOpConversion final : public ConversionPattern {
public:
  explicit WriteOpConversion(MLIRContext *context)
      : ConversionPattern(stencil::WriteOp::getOperationName(), 1, context) {}

  PatternMatchResult
  matchAndRewrite(Operation *operation, ArrayRef<Value *> operands,
                  ConversionPatternRewriter &rewriter) const override {
    auto loc = operation->getLoc();
    auto target = cast<LoadOp>(operands[0]->getDefiningOp()).memref();

    // TODO: Refactor this together with LoadOp conversion

    auto memref = target->getType().cast<MemRefType>();
    if (memref.getShape().size() == 0) {
      // Variable
      rewriter.replaceOpWithNewOp<StoreOp>(operation, operands[1], target,
                                           llvm::None);
    } else if (memref.getShape().size() == 1) {
      // Field
      // Update these according to the generated loop order
      // Assume JIK order here
      auto writeOp = cast<stencil::WriteOp>(operation);
      auto fieldAccess = cast<stencil::FieldAccessOp>(writeOp.target()->getDefiningOp());
      auto offset = fieldAccess.offset().getValue();

      if (offset.size() != 3) {
        operation->emitError("expected offset to have three components");
        return matchFailure();
      }

      int64_t ioffsetAttr = offset[0].cast<IntegerAttr>().getInt();
      int64_t joffsetAttr = offset[1].cast<IntegerAttr>().getInt();
      int64_t koffsetAttr = offset[2].cast<IntegerAttr>().getInt();

      auto ioffset =
          rewriter.create<ConstantIndexOp>(loc, ioffsetAttr).getResult();
      auto joffset =
          rewriter.create<ConstantIndexOp>(loc, joffsetAttr).getResult();
      auto koffset =
          rewriter.create<ConstantIndexOp>(loc, koffsetAttr).getResult();

      auto kLoop = operation->getParentOfType<AffineForOp>();
      if (!kLoop) {
        operation->emitError(
            "expected field access to be enclosed in `k` loop");
        return matchFailure();
      }
      auto iLoop = kLoop.getParentOfType<AffineForOp>();
      if (!iLoop) {
        operation->emitError("expected `k` loop to be enclosed in `i` loop");
        return matchFailure();
      }
      auto jLoop = iLoop.getParentOfType<AffineForOp>();
      if (!jLoop) {
        operation->emitError("expected `i` loop to be enclosed in `j` loop");
        return matchFailure();
      }

      auto i = rewriter.create<AddIOp>(loc, iLoop.getInductionVar(), ioffset);
      auto j = rewriter.create<AddIOp>(loc, jLoop.getInductionVar(), joffset);
      auto kVar = kLoop.getInductionVar();
      Value *realKValue = getRealKValue(kVar);
      if (!realKValue) {
        operation->emitError(
            "expected 'k' index to be computed by an affine apply operation");
        return matchFailure();
      }
      auto k = rewriter.create<AddIOp>(loc, realKValue, koffset);

      auto stencilFunc = operation->getParentOfType<FuncOp>();
      if (!stencilFunc) {
        operation->emitError("expected parent function operation");
        return matchFailure();
      }
      auto isize = stencilFunc.getArgument(0);
      auto jsize = stencilFunc.getArgument(1);
      auto ksize = stencilFunc.getArgument(2);
      auto boundarySize = stencilFunc.getArgument(3);

      auto arrayOffset = computeOffset(i, j, k, isize, jsize, ksize, boundarySize,
                                       rewriter, loc);

      target->setType(MemRefType::get(-1, memref.getElementType()));
      rewriter.replaceOpWithNewOp<StoreOp>(operation, operands[1], target,
                                           llvm::makeArrayRef({arrayOffset}));
    } else {
      operation->emitError("Unsupported stencil memref shape");
      return matchFailure();
    }

    return matchSuccess();
  }
};

class GetValueOpConversion final : public ConversionPattern {
public:
  explicit GetValueOpConversion(MLIRContext *context)
      : ConversionPattern(stencil::GetValueOp::getOperationName(), 1, context) {
  }

  PatternMatchResult
  matchAndRewrite(Operation *operation, ArrayRef<Value *> operands,
                  ConversionPatternRewriter &rewriter) const override {
    rewriter.replaceOp(operation, operands[0]);

    return matchSuccess();
  }
};

} // namespace

void mlir::populateStencilToAffineConversionPatterns_2(
    OwningRewritePatternList &patterns, MLIRContext *context) {
  patterns
      .insert<StencilOpConversion, IIROpConversion, FieldOpConversion,
              VarOpConversion, FieldAccessOpConversion, VarAccessOpConversion,
              WriteOpConversion, GetValueOpConversion, StencilEndOpConversion>(
          context);
}
