//===- ConvertStandardToAffine.cpp - Stencil to Affine dialect conversion -===//
//
// Copyright 2019 Fabian Wolff
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// =============================================================================
//
// This file implements a pass to convert the Stencil dialect's structure
// operations into affine constructs.
//
//===----------------------------------------------------------------------===//

#include "mlir/Conversion/StencilToAffine/ConvertStencilToAffine.h"
#include "mlir/Dialect/AffineOps/AffineOps.h"
#include "mlir/Dialect/StandardOps/Ops.h"
#include "mlir/IR/AffineExpr.h"
#include "mlir/IR/AffineMap.h"
#include "mlir/IR/Builders.h"
#include "mlir/IR/MLIRContext.h"
#include "mlir/IR/Module.h"
#include "mlir/IR/PatternMatch.h"
#include "mlir/IR/StandardTypes.h"
#include "mlir/Pass/Pass.h"
#include "mlir/Support/Functional.h"
#include "mlir/Transforms/DialectConversion.h"
#include "mlir/Transforms/Passes.h"
#include "mlir/Transforms/Utils.h"

#define KEND_MAGIC 1048576

using namespace mlir;

static const char kMultiStageLoopOrderAttrName[] = "multiStageLoopOrder";

namespace {

struct StencilToAffinePass : public ModulePass<StencilToAffinePass> {
  void runOnModule() override;
};

/* This "hidden" extra pass cleans up any useless empty loop nests that might
 * have been generated during the lowering.
 */
struct StencilToAffineCleanupPass
    : public ModulePass<StencilToAffineCleanupPass> {
  void runOnModule() override;
};

struct DoMethodLowering final : public OpRewritePattern<stencil::DoMethodOp> {
  using OpRewritePattern<stencil::DoMethodOp>::OpRewritePattern;

  PatternMatchResult matchAndRewrite(stencil::DoMethodOp doMethodOp,
                                     PatternRewriter &rewriter) const override;
};

struct StageLowering final : public OpRewritePattern<stencil::StageOp> {
  using OpRewritePattern<stencil::StageOp>::OpRewritePattern;

  PatternMatchResult matchAndRewrite(stencil::StageOp stageOp,
                                     PatternRewriter &rewriter) const override;
};

struct MultiStageLowering final
    : public OpRewritePattern<stencil::MultiStageOp> {
  using OpRewritePattern<stencil::MultiStageOp>::OpRewritePattern;

  PatternMatchResult matchAndRewrite(stencil::MultiStageOp stageOp,
                                     PatternRewriter &rewriter) const override;
};

struct RemoveEmptyAffineForOps final : public OpRewritePattern<AffineForOp> {
  using OpRewritePattern<AffineForOp>::OpRewritePattern;

  PatternMatchResult matchAndRewrite(AffineForOp stageOp,
                                     PatternRewriter &rewriter) const override;
};

} // anonymous namespace

PatternMatchResult
DoMethodLowering::matchAndRewrite(stencil::DoMethodOp doMethodOp,
                                  PatternRewriter &rewriter) const {
  Location loc = doMethodOp.getLoc();
  MLIRContext *ctx = doMethodOp.getContext();

  auto interval = doMethodOp.getInterval();
  auto &lowerLevel = interval.lowerLevel;
  auto &lowerOffset = interval.lowerOffset;
  auto &upperLevel = interval.upperLevel;
  auto &upperOffset = interval.upperOffset;

  auto loopOrderAttr = stencil::symbolizeLoopOrder(
      doMethodOp.getAttr(kMultiStageLoopOrderAttrName)
          .cast<StringAttr>()
          .getValue());
  if (!loopOrderAttr) {
    doMethodOp.emitError("expected 'multiStageLoopOrder' attribute");
    return matchFailure();
  }
  // Multi stages define the iteration order
  bool backward = loopOrderAttr.getValue() == stencil::LoopOrder::Backward;

  auto stencilFunc = doMethodOp.getParentOfType<FuncOp>();
  if (!stencilFunc) {
    doMethodOp.emitError("expected parent function operation");
    return matchFailure();
  }

  Value *kLowerBound;
  Value *kUpperBound;
  Value *ksize = stencilFunc.getArgument(2);

  if (lowerLevel == KEND_MAGIC) {
    // In this case, lowerBound = kend = ksize - 1
    AffineExpr decrementExpr = getAffineDimExpr(0, ctx) - 1;
    auto decrementMap = AffineMap::get(1, 0, decrementExpr);
    kLowerBound = rewriter
                      .create<AffineApplyOp>(loc, decrementMap,
                                             llvm::makeArrayRef({ksize}))
                      .getResult();
  } else {
    kLowerBound = rewriter.create<ConstantIndexOp>(loc, lowerLevel).getResult();
  }

  if (upperLevel == KEND_MAGIC) {
    // In this case, upperBound = kend = ksize - 1
    AffineExpr decrementExpr = getAffineDimExpr(0, ctx) - 1;
    auto decrementMap = AffineMap::get(1, 0, decrementExpr);
    kUpperBound = rewriter
                      .create<AffineApplyOp>(loc, decrementMap,
                                             llvm::makeArrayRef({ksize}))
                      .getResult();
  } else {
    kUpperBound = rewriter.create<ConstantIndexOp>(loc, upperLevel).getResult();
  }

  AffineExpr affExprLo = getAffineDimExpr(0, ctx) + lowerOffset;
  AffineExpr affExprUp = getAffineDimExpr(0, ctx) + upperOffset;

  AffineExpr upperBoundExpr =
      affExprUp + 1; // Account for half-open ranges in affine for operations.
                     // Do methods use closed ranges.

  auto kLoop = rewriter.create<AffineForOp>(
      loc, llvm::makeArrayRef({kLowerBound}),
      simplifyAffineMap(AffineMap::get(1, 0, affExprLo)),
      llvm::makeArrayRef({kUpperBound}),
      simplifyAffineMap(AffineMap::get(1, 0, upperBoundExpr)), 1);

  doMethodOp.getOperation()->walk([&rewriter](stencil::DoMethodEndOp op) {
    rewriter.setInsertionPointAfter(op.getOperation());
    rewriter.create<AffineTerminatorOp>(op.getOperation()->getLoc());
    rewriter.eraseOp(op.getOperation());
  });

  doMethodOp.getOperation()->getRegion(0).front().addArgument(
      IndexType::get(ctx));
  kLoop.getOperation()->getRegion(0).takeBody(
      doMethodOp.getOperation()->getRegion(0));

  SmallVector<Value *, 1> lowerBoundOperands;
  for (auto operand : kLoop.getLowerBoundOperands()) {
    lowerBoundOperands.push_back(operand);
  }
  SmallVector<Value *, 1> upperBoundOperands;
  for (auto operand : kLoop.getUpperBoundOperands()) {
    upperBoundOperands.push_back(operand);
  }

  rewriter.setInsertionPointToStart(kLoop.getBody());
  auto lowerBound = rewriter
                        .create<AffineApplyOp>(loc, kLoop.getLowerBoundMap(),
                                               lowerBoundOperands)
                        .getResult();
  auto upperBound = rewriter
                        .create<AffineApplyOp>(loc, kLoop.getUpperBoundMap(),
                                               upperBoundOperands)
                        .getResult();
  if (backward) {
    AffineExpr invertIndex =
        getAffineDimExpr(0, ctx) - getAffineConstantExpr(1, ctx) -
        getAffineDimExpr(1, ctx) + getAffineDimExpr(2, ctx);
    auto affineMap = AffineMap::get(3, 0, invertIndex);
    rewriter.create<AffineApplyOp>(
        loc, simplifyAffineMap(affineMap),
        llvm::makeArrayRef({upperBound, kLoop.getInductionVar(), lowerBound}));
  } else {
    auto affineMap = AffineMap::getMultiDimIdentityMap(1, ctx);
    rewriter.create<AffineApplyOp>(
        loc, simplifyAffineMap(affineMap),
        llvm::makeArrayRef({kLoop.getInductionVar()}));
  }

  rewriter.replaceOp(doMethodOp, llvm::None);

  return matchSuccess();
}

PatternMatchResult
StageLowering::matchAndRewrite(stencil::StageOp stageOp,
                               PatternRewriter &rewriter) const {
  Location loc = stageOp.getLoc();
  MLIRContext *ctx = stageOp.getContext();

  rewriter.setInsertionPoint(stageOp.getOperation());

  // Compute the upper bound of the loop by subtracting twice the boundary size
  // from the i and j sizes. We only have boundaries on i and j, so this is not
  // needed for the k loop.

  auto stencilFunc = stageOp.getParentOfType<FuncOp>();
  if (!stencilFunc) {
    stageOp.emitError("expected parent function operation");
    return matchFailure();
  }
  auto lowerBound = rewriter.create<ConstantIndexOp>(loc, 0).getResult();
  auto identityMap = AffineMap::getMultiDimIdentityMap(1, ctx);

  Value *isize = stencilFunc.getArgument(0);
  Value *jsize = stencilFunc.getArgument(1);
  Value *boundary = stencilFunc.getArgument(3);

  AffineExpr boundaryComputation =
      getAffineDimExpr(0, ctx) -
      getAffineConstantExpr(2, ctx) * getAffineDimExpr(1, ctx);
  auto boundaryComputationMap =
      simplifyAffineMap(AffineMap::get(2, 0, boundaryComputation));
  auto iUpperBound =
      rewriter
          .create<AffineApplyOp>(loc, boundaryComputationMap,
                                 llvm::makeArrayRef({isize, boundary}))
          .getResult();
  auto jUpperBound =
      rewriter
          .create<AffineApplyOp>(loc, boundaryComputationMap,
                                 llvm::makeArrayRef({jsize, boundary}))
          .getResult();

  auto jForOp = rewriter.create<AffineForOp>(
      loc, llvm::makeArrayRef({lowerBound}), identityMap,
      llvm::makeArrayRef({jUpperBound}), identityMap, 1);
  rewriter.setInsertionPointToStart(jForOp.getBody());
  auto iForOp = rewriter.create<AffineForOp>(
      loc, llvm::makeArrayRef({lowerBound}), identityMap,
      llvm::makeArrayRef({iUpperBound}), identityMap, 1);

  stageOp.getOperation()->walk([&rewriter](stencil::StageEndOp op) {
    rewriter.setInsertionPointAfter(op.getOperation());
    rewriter.create<AffineTerminatorOp>(op.getOperation()->getLoc());
    rewriter.eraseOp(op.getOperation());
  });

  stageOp.getOperation()->getRegion(0).front().addArgument(IndexType::get(ctx));
  iForOp.getOperation()->getRegion(0).takeBody(
      stageOp.getOperation()->getRegion(0));

  rewriter.replaceOp(stageOp, llvm::None);

  return matchSuccess();
}

PatternMatchResult
MultiStageLowering::matchAndRewrite(stencil::MultiStageOp multiStageOp,
                                    PatternRewriter &rewriter) const {
  rewriter.setInsertionPointAfter(multiStageOp.getOperation());

  for (auto &nestedOp : multiStageOp.getBody()) {
    if (!isa<stencil::MultiStageEndOp>(nestedOp))
      rewriter.clone(nestedOp);
  }

  rewriter.replaceOp(multiStageOp, llvm::None);

  return matchSuccess();
}

static bool isEmptyAffineForOp(AffineForOp affForOp) {
  for (auto &op : *(affForOp.getBody())) {
    if (llvm::isa<AffineTerminatorOp>(op))
      continue;
    else if (llvm::isa<AffineForOp>(op)) {
      if (isEmptyAffineForOp(llvm::dyn_cast<AffineForOp>(op)))
        continue;
      else
        return false;
    } else
      return false;
  }

  return true;
}

PatternMatchResult
RemoveEmptyAffineForOps::matchAndRewrite(AffineForOp affForOp,
                                         PatternRewriter &rewriter) const {
  if (isEmptyAffineForOp(affForOp)) {
    rewriter.replaceOp(affForOp, llvm::None);
    return matchSuccess();
  } else
    return matchFailure();
}

void mlir::populateStencilToAffineConversionPatterns(
    OwningRewritePatternList &patterns, MLIRContext *ctx) {
  patterns.insert<DoMethodLowering>(ctx);
  patterns.insert<StageLowering>(ctx);
  patterns.insert<MultiStageLowering>(ctx);
  patterns.insert<RemoveEmptyAffineForOps>(ctx);
}

static void populateStencilToAffineCleanupConversionPatterns(
    OwningRewritePatternList &patterns, MLIRContext *ctx) {
  patterns.insert<RemoveEmptyAffineForOps>(ctx);
}

void StencilToAffinePass::runOnModule() {
  OwningRewritePatternList patterns;
  auto module = getModule();

  module.walk([](stencil::IIROp iirOp) {
    iirOp.walk([&](stencil::StencilOp stencilOp) {
      stencilOp.setAttr("stencilName", StringAttr::get(iirOp.getStencilName(),
                                                       stencilOp.getContext()));
    });
  });

  populateStencilToAffineConversionPatterns(patterns, module.getContext());
  populateStencilToAffineConversionPatterns_2(patterns, module.getContext());
  ConversionTarget target(*(module.getContext()));
  target.addLegalDialect<AffineOpsDialect>();
  target.addLegalDialect<StandardOpsDialect>();
  target.addLegalOp<FuncOp>();

  // Propagate loop order from multi-stage ops to do methods
  auto &context = getContext();
  module.walk([&context](stencil::MultiStageOp multiStageOp) {
    multiStageOp.walk([&](stencil::DoMethodOp doMethodOp) {
      doMethodOp.setAttr(
          kMultiStageLoopOrderAttrName,
          StringAttr::get(stencil::stringifyLoopOrder(multiStageOp.loopOrder()),
                          &context));
    });
  });

  if (failed(applyPartialConversion(module, target, patterns)))
    signalPassFailure();

  // We need to run the conversion twice: The first one replaces the
  // multi-stages, but since the rewriter does not touch the newly generated
  // operations (cloned from the multi-stage body), we need to run it again
  if (failed(applyPartialConversion(module, target, patterns)))
    signalPassFailure();

  OwningRewritePatternList cleanupPatterns;
  populateStencilToAffineCleanupConversionPatterns(cleanupPatterns,
                                                   module.getContext());
  ConversionTarget cleanupTarget(*(module.getContext()));

  if (failed(applyPartialConversion(module, cleanupTarget, cleanupPatterns)))
    signalPassFailure();
}

static PassRegistration<StencilToAffinePass>
    pass("convert-stencil-to-affine",
         "Convert Stencil dialect to Affine dialect, "
         "replacing abstract stencils with affine loop nests");
