//===- ConvertIIRToStencil.cpp - IIR to Stencil dialect conversion --------===//
//
// Copyright 2019 Jean-Michel Gorius
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// =============================================================================
//
// This file implements a pass to convert the MLIR IIR dialect into the Stencil
// dialect.
//
//===----------------------------------------------------------------------===//

#include "mlir/Conversion/IIRToStencil/ConvertIIRToStencil.h"
#include "mlir/Dialect/LoopOps/LoopOps.h"
#include "mlir/Dialect/StandardOps/Ops.h"
#include "mlir/Dialect/Stencil/IIR/IIRAttributes.h"
#include "mlir/Dialect/Stencil/IIR/IIROps.h"
#include "mlir/Dialect/Stencil/Stencil/StencilAttributes.h"
#include "mlir/Dialect/Stencil/Stencil/StencilDialect.h"
#include "mlir/Dialect/Stencil/Stencil/StencilOps.h"
#include "llvm/ADT/StringMap.h"

#include <llvm/Support/raw_ostream.h>
#include <stack>
#include <string>
#include <unordered_map>
#include <utility>

using namespace mlir;

//===----------------------------------------------------------------------===//
// Common utilities
//===----------------------------------------------------------------------===//

Value *dereferenceIfPointer(Value *ptr, OpBuilder &builder, Location loc) {
  if (ptr->getType().isa<stencil::PointerType>())
    return builder.create<stencil::GetValueOp>(loc, ptr).getResult();
  else
    return ptr;
}

//===----------------------------------------------------------------------===//
// Type Conversion
//===----------------------------------------------------------------------===//

Type StencilBasicTypeConverter::convertType(Type t) {
  if (stencil::StencilDialect::isValidType(t))
    return t;
  return Type();
}

//===----------------------------------------------------------------------===//
// Operation conversion
//===----------------------------------------------------------------------===//

namespace {

using ValueStack = std::stack<Value *>;

void convertOperation(Operation *op, OpBuilder &builder, ValueStack &stack,
                      bool ignoreIfBlocks = true);

void convert(iir::LiteralAccessExprOp literalAccessExprOp, OpBuilder &builder,
             ValueStack &stack) {
  auto op = literalAccessExprOp.getOperation();
  auto builtinType = literalAccessExprOp.builtinType();
  auto value = literalAccessExprOp.value();

  Type retType;
  Attribute valueAttr;
  switch (builtinType) {
  case iir::BuiltinType::Boolean: {
    retType = builder.getI1Type();
    if (value == "false") {
      valueAttr = builder.getBoolAttr(false);
    } else if (value == "true") {
      valueAttr = builder.getBoolAttr(true);
    } else {
      literalAccessExprOp.emitError(Twine("Unknown boolean value ", value));
      return;
    }
    break;
  }
  case iir::BuiltinType::Integer: {
    int64_t ival = 0;
    retType = builder.getIntegerType(64);
    value.getAsInteger(10, ival);
    valueAttr = builder.getI64IntegerAttr(ival);
    break;
  }
  case iir::BuiltinType::Float: {
    double fval = 0.0;
    retType = builder.getF64Type();
    value.getAsDouble(fval);
    valueAttr = builder.getF64FloatAttr(fval);
    break;
  }
  case iir::BuiltinType::Auto:
    literalAccessExprOp.emitError("Auto type not supported");
    break;
  case iir::BuiltinType::Invalid:
    literalAccessExprOp.emitError("Invalid IIR builtin type");
    break;
  }

  // Workaround implicit conversions in the IIR
  if (retType.isInteger(64)) {
    auto constantIntOp = builder.create<ConstantOp>(
        op->getLoc(), builder.getIntegerType(64), valueAttr);
    auto convertOp = builder.create<SIToFPOp>(
        op->getLoc(), constantIntOp.getResult(), builder.getF64Type());
    stack.push(convertOp.getResult());
  } else {
    auto constantOp =
        builder.create<ConstantOp>(op->getLoc(), retType, valueAttr);
    stack.push(constantOp.getResult());
  }
}

void convert(iir::FieldAccessExprOp fieldAccessExprOp, OpBuilder &builder,
             ValueStack &stack) {
  auto op = fieldAccessExprOp.getOperation();
  auto name = fieldAccessExprOp.name();
  auto offset = fieldAccessExprOp.offset();

  auto stencilOp = op->getParentOfType<stencil::StencilOp>();
  if (!stencilOp) {
    fieldAccessExprOp.emitError("expected StencilOp parent operation");
    return;
  }
  auto argNamesAttr = stencilOp.getAttr("argNames");
  if (!argNamesAttr) {
    fieldAccessExprOp.emitError("expected 'argNames' attribute");
    return;
  }
  auto argNames = argNamesAttr.cast<ArrayAttr>().getValue();

  // Check if the accessed field is an API field
  int fieldIndex = -1;
  for (int i = 0, e = argNames.size(); i < e; ++i) {
    auto argName = argNames[i].cast<StringAttr>().getValue();
    if (argName == name) {
      fieldIndex = i;
      break;
    }
  }

  Value *field = nullptr;
  if (fieldIndex >= 0)
    field = stencilOp.getBody().getArgument(fieldIndex);
  else {
    stencilOp.walk([&](stencil::FieldOp fieldOp) {
      auto fieldName = fieldOp.name();
      if (fieldName == name)
        field = fieldOp.getResult();
    });
    if (!field) {
      fieldAccessExprOp.emitError("unknown non-API field");
      return;
    }
  }

  auto fieldAccessOp =
      builder.create<stencil::FieldAccessOp>(op->getLoc(), field, offset);

  stack.push(fieldAccessOp.getResult());
}

void convert(iir::VarAccessExprOp varAccessExprOp, OpBuilder &builder,
             ValueStack &stack) {
  auto op = varAccessExprOp.getOperation();
  auto name = varAccessExprOp.name();

  if (varAccessExprOp.getAttr("isArray")) {
    stack.pop(); // Drop the index
    varAccessExprOp.emitError("Array local variables are not supported");
    return;
  }

  auto stencilOp = op->getParentOfType<stencil::StencilOp>();
  if (!stencilOp) {
    varAccessExprOp.emitError("expected StencilOp parent operation");
    return;
  }

  Value *var = nullptr;
  stencilOp.walk([&](stencil::VarOp varOp) {
    if (varOp.name() == name) {
      var = varOp.getResult();
      return WalkResult::interrupt();
    } else
      return WalkResult::advance();
  });
  if (!var) {
    varAccessExprOp.emitError(Twine("unknown variable ", name));
    return;
  }

  auto varAccessOp = builder.create<stencil::VarAccessOp>(op->getLoc(), var);

  stack.push(varAccessOp.getResult());
}

void convert(iir::BinaryOperatorOp binaryOperatorOp, OpBuilder &builder,
             ValueStack &stack) {
  auto op = binaryOperatorOp.getOperation();
  auto binaryOperator = binaryOperatorOp.op();

  if (stack.size() < 2) {
    binaryOperatorOp.emitError("binary operator expects two operands");
    return;
  }
  auto rhs = dereferenceIfPointer(stack.top(), builder, op->getLoc());
  stack.pop();
  auto lhs = dereferenceIfPointer(stack.top(), builder, op->getLoc());
  stack.pop();

  Value *result = nullptr;
  switch (binaryOperator) {
  case iir::BinaryOperator::Plus:
    result = builder.create<AddFOp>(op->getLoc(), lhs, rhs).getResult();
    break;
  case iir::BinaryOperator::Minus:
    result = builder.create<SubFOp>(op->getLoc(), lhs, rhs).getResult();
    break;
  case iir::BinaryOperator::Times:
    result = builder.create<MulFOp>(op->getLoc(), lhs, rhs).getResult();
    break;
  case iir::BinaryOperator::Divide:
    result = builder.create<DivFOp>(op->getLoc(), lhs, rhs).getResult();
    break;
  case iir::BinaryOperator::Lt:
    result = builder.create<CmpFOp>(op->getLoc(), CmpFPredicate::ULT, lhs, rhs)
                 .getResult();
    break;
  case iir::BinaryOperator::Gt:
    result = builder.create<CmpFOp>(op->getLoc(), CmpFPredicate::UGT, lhs, rhs)
                 .getResult();
    break;
  case iir::BinaryOperator::Le:
    result = builder.create<CmpFOp>(op->getLoc(), CmpFPredicate::ULE, lhs, rhs)
                 .getResult();
    break;
  case iir::BinaryOperator::Ge:
    result = builder.create<CmpFOp>(op->getLoc(), CmpFPredicate::UGE, lhs, rhs)
                 .getResult();
    break;
  case iir::BinaryOperator::IsEqual:
    result = builder.create<CmpFOp>(op->getLoc(), CmpFPredicate::UEQ, lhs, rhs)
                 .getResult();
    break;
  case iir::BinaryOperator::And:
    result = builder.create<AndOp>(op->getLoc(), lhs, rhs).getResult();
    break;
  case iir::BinaryOperator::Or:
    result = builder.create<OrOp>(op->getLoc(), lhs, rhs).getResult();
    break;
  }

  stack.push(result);
}

void convert(iir::UnaryOperatorOp unaryOperatorOp, OpBuilder &builder,
             ValueStack &stack) {
  auto op = unaryOperatorOp.getOperation();
  auto unaryOperator = unaryOperatorOp.op();

  if (stack.empty()) {
    unaryOperatorOp.emitError("unary operator expects one operand");
    return;
  }

  auto arg = dereferenceIfPointer(stack.top(), builder, op->getLoc());
  stack.pop();

  Value *result = nullptr;
  switch (unaryOperator) {
  case iir::UnaryOperator::UnaryMinus:
    result = builder.create<ConstantOp>(op->getLoc(), arg->getType(),
                                        builder.getZeroAttr(arg->getType()));
    result = builder.create<SubFOp>(op->getLoc(), result, arg);
    break;
  }

  stack.push(result);
}

void convert(iir::AssignmentExprOp assignmentExprOp, OpBuilder &builder,
             ValueStack &stack) {
  auto op = assignmentExprOp.getOperation();

  if (stack.size() < 2) {
    assignmentExprOp.emitError("write operation expects two operands");
    return;
  }
  auto value = dereferenceIfPointer(stack.top(), builder, op->getLoc());
  stack.pop();

  auto target = stack.top();
  stack.pop();
  if (!target->getType().isa<stencil::PointerType>()) {
    assignmentExprOp.emitError("write target should be a pointer");
    return;
  }

  switch (assignmentExprOp.op()) {
  case iir::AssignmentOperator::Equal:
    break;
  case iir::AssignmentOperator::PlusEqual: {
    auto getValueOp = builder.create<stencil::GetValueOp>(op->getLoc(), target);
    auto addOp =
        builder.create<AddFOp>(op->getLoc(), getValueOp.getResult(), value);
    value = addOp.getResult();
  } break;
  case iir::AssignmentOperator::MinusEqual: {
    auto getValueOp = builder.create<stencil::GetValueOp>(op->getLoc(), target);
    auto subOp =
        builder.create<SubFOp>(op->getLoc(), getValueOp.getResult(), value);
    value = subOp.getResult();
  } break;
  case iir::AssignmentOperator::TimesEqual: {
    auto getValueOp = builder.create<stencil::GetValueOp>(op->getLoc(), target);
    auto mulOp =
        builder.create<MulFOp>(op->getLoc(), getValueOp.getResult(), value);
    value = mulOp.getResult();
  } break;
  case iir::AssignmentOperator::DivideEqual: {
    auto getValueOp = builder.create<stencil::GetValueOp>(op->getLoc(), target);
    auto divOp =
        builder.create<DivFOp>(op->getLoc(), getValueOp.getResult(), value);
    value = divOp.getResult();
  } break;
  }

  builder.create<stencil::WriteOp>(op->getLoc(), target, value);
}

void convert(iir::StencilOp iirStencilOp, OpBuilder &builder,
             const SmallVectorImpl<Type> &apiFieldTypes,
             const SmallVectorImpl<StringRef> &apiFieldNames) {
  auto op = iirStencilOp.getOperation();

  SmallVector<stencil::StencilAttribute, 3> stencilAttributes;
  if (iirStencilOp.getAttr("mergeDoMethods"))
    stencilAttributes.push_back(stencil::StencilAttribute::MergeDoMethods);
  if (iirStencilOp.getAttr("mergeStages"))
    stencilAttributes.push_back(stencil::StencilAttribute::MergeStages);
  if (iirStencilOp.getAttr("mergeTemporaries"))
    stencilAttributes.push_back(stencil::StencilAttribute::MergeTemporaries);
  if (iirStencilOp.getAttr("noCodeGen"))
    stencilAttributes.push_back(stencil::StencilAttribute::NoCodeGen);
  if (iirStencilOp.getAttr("useKCaches"))
    stencilAttributes.push_back(stencil::StencilAttribute::UseKCaches);

  auto stencilOp =
      builder.create<stencil::StencilOp>(op->getLoc(), stencilAttributes);
  stencilOp.getOperation()->getRegion(0).takeBody(op->getRegion(0));
  stencilOp.getBody().addArguments(apiFieldTypes);
  builder.setInsertionPointToStart(&stencilOp.getBody());

  llvm::SmallVector<StringRef, 3> fields;
  stencilOp.walk([&](iir::FieldAccessExprOp fieldAccessExprOp) {
    StringRef name = fieldAccessExprOp.name();
    // If the field is not an API field
    if (!llvm::is_contained(fields, name) &&
        !llvm::is_contained(apiFieldNames, name)) {
      builder.create<stencil::FieldOp>(op->getLoc(), name);
      fields.push_back(name);
    }
  });

  llvm::SmallVector<StringRef, 3> variables;
  stencilOp.walk([&](iir::VarDeclStatementOp varDeclStatementOp) {
    auto nameAttr = varDeclStatementOp.getAttr("name");
    if (!nameAttr) {
      varDeclStatementOp.emitError("expected 'name' attribute");
      return;
    }
    StringRef name = nameAttr.cast<StringAttr>().getValue();
    if (!llvm::is_contained(variables, name)) {
      auto doMethodOp = varDeclStatementOp.getParentOfType<iir::StageOp>();
      assert(doMethodOp && "expected parent do method operation");
      builder.setInsertionPointToStart(&doMethodOp.getBody());
      builder.create<stencil::VarOp>(op->getLoc(), name);
      variables.push_back(name);
    }
  });

  stencilOp.setAttr("argNames", builder.getStrArrayAttr(apiFieldNames));
}

void convert(iir::VarDeclStatementOp varDeclStatementOp, OpBuilder &builder,
             ValueStack &stack) {
  auto op = varDeclStatementOp.getOperation();

  auto varNameAttr = varDeclStatementOp.getAttr("name");
  if (!varNameAttr) {
    varDeclStatementOp.emitError("expected 'name' attribute");
    return;
  }
  auto varName = varNameAttr.cast<StringAttr>().getValue();

  // Search for the corresponding value
  Value *var = nullptr;
  auto stageOp = op->getParentOfType<stencil::StageOp>();
  if (!stageOp) {
    varDeclStatementOp.emitError(
        "expected stencil::DoMethodOp parent operation");
    return;
  }
  stageOp.walk([&](stencil::VarOp varOp) {
    if (varOp.name() == varName) {
      var = varOp.getResult();
      return WalkResult::interrupt();
    } else
      return WalkResult::advance();
  });
  if (!var) {
    varDeclStatementOp.emitError(Twine("unknown local variable", varName));
    return;
  }

  auto target = builder.create<stencil::VarAccessOp>(op->getLoc(), var);

  if (stack.empty()) {
    varDeclStatementOp.emitError(
        "variable declaration operation expects one operand");
    return;
  }
  auto value = dereferenceIfPointer(stack.top(), builder, op->getLoc());
  stack.pop();

  builder.create<stencil::WriteOp>(op->getLoc(), target, value);
}

void convert(iir::FunCallOp functionCallOp, OpBuilder &builder,
             ValueStack &stack) {
  auto op = functionCallOp.getOperation();
  auto callee = functionCallOp.callee();

  if (callee == "gridtools::clang::math::min") {
    if (stack.size() < 2) {
      functionCallOp.emitError("'min' requires two arguments");
      return;
    }
    auto rhs = dereferenceIfPointer(stack.top(), builder, op->getLoc());
    stack.pop();
    auto lhs = dereferenceIfPointer(stack.top(), builder, op->getLoc());
    stack.pop();

    auto cmpRes =
        builder.create<CmpFOp>(op->getLoc(), CmpFPredicate::ULT, lhs, rhs)
            .getResult();
    auto min = builder.create<SelectOp>(op->getLoc(), cmpRes, lhs, rhs);
    stack.push(min);
  } else if (callee == "gridtools::clang::math::max") {
    if (stack.size() < 2) {
      functionCallOp.emitError("'max' requires two arguments");
      return;
    }
    auto rhs = dereferenceIfPointer(stack.top(), builder, op->getLoc());
    stack.pop();
    auto lhs = dereferenceIfPointer(stack.top(), builder, op->getLoc());
    stack.pop();

    auto cmpRes =
        builder.create<CmpFOp>(op->getLoc(), CmpFPredicate::UGT, lhs, rhs)
            .getResult();
    auto min = builder.create<SelectOp>(op->getLoc(), cmpRes, lhs, rhs);
    stack.push(min);
  } else if (callee == "gridtools::clang::math::sqrt") {
    if (stack.empty()) {
      functionCallOp.emitError("'sqrt' requires one argument");
      return;
    }
    auto value = dereferenceIfPointer(stack.top(), builder, op->getLoc());
    stack.pop();

    auto sqrt =
        builder.create<stencil::SqrtfOp>(op->getLoc(), value).getResult();
    stack.push(sqrt);
  } else if (callee == "gridtools::clang::math::fabs" || callee == "fabs") {
    // We need the 'fabs' case here because of a gridtools template
    // instantiation bug. Some stencils use 'fabs' instead of 'math::fabs'
    // because they call the functions on gridtools variables, which causes a
    // type mismatch error.
    if (stack.empty()) {
      functionCallOp.emitError("'fabs' requires one argument");
      return;
    }
    auto value = dereferenceIfPointer(stack.top(), builder, op->getLoc());
    stack.pop();

    auto fabs =
        builder.create<stencil::FabsOp>(op->getLoc(), value).getResult();
    stack.push(fabs);
  } else if (callee == "gridtools::clang::math::exp") {
    if (stack.empty()) {
      functionCallOp.emitError("'exp' requires one argument");
      return;
    }
    auto value = dereferenceIfPointer(stack.top(), builder, op->getLoc());
    stack.pop();

    auto exp = builder.create<stencil::ExpOp>(op->getLoc(), value).getResult();
    stack.push(exp);
  } else if (callee == "gridtools::clang::math::pow") {
    if (stack.size() < 2) {
      functionCallOp.emitError("'pow' requires two arguments");
      return;
    }
    auto exponent = dereferenceIfPointer(stack.top(), builder, op->getLoc());
    stack.pop();
    auto value = dereferenceIfPointer(stack.top(), builder, op->getLoc());
    stack.pop();

    auto pow = builder.create<stencil::PowOp>(op->getLoc(), value, exponent)
                   .getResult();
    stack.push(pow);
  } else
    functionCallOp.emitError(Twine("Unsupport function ", callee));
}

void convert(iir::IfOp ifOp, OpBuilder &builder, ValueStack &stack) {
  auto op = ifOp.getOperation();
  auto loc = op->getLoc();

  ifOp.getCond().walk([&](Operation *operation) {
    convertOperation(operation, builder, stack, false);
  });

  if (stack.empty()) {
    ifOp.emitError("expected condition variable");
    return;
  }

  // Get condition variable
  auto cond = stack.top();
  stack.pop();

  auto &thenBlock = ifOp.getThen();
  auto &elseBlock = ifOp.getElse();
  bool emptyElse = elseBlock.getOperations().size() == 1;

  auto newIfOp = builder.create<loop::IfOp>(loc, cond, !emptyElse);
  auto thenBuilder = newIfOp.getThenBodyBuilder();
  thenBlock.walk(
      [&](Operation *op) { convertOperation(op, thenBuilder, stack, false); });
  if (!emptyElse) {
    auto elseBuilder = newIfOp.getElseBodyBuilder();
    elseBlock.walk([&](Operation *op) {
      convertOperation(op, elseBuilder, stack, false);
    });
  }
}

void convert(iir::TernaryOperatorOp ternaryOperatorOp, OpBuilder &builder,
             ValueStack &stack) {
  auto op = ternaryOperatorOp.getOperation();
  auto loc = op->getLoc();

  if (stack.size() < 3) {
    ternaryOperatorOp.emitError("expected three operands");
    return;
  }

  auto falseValue = dereferenceIfPointer(stack.top(), builder, loc);
  stack.pop();
  auto trueValue = dereferenceIfPointer(stack.top(), builder, loc);
  stack.pop();
  auto cond = stack.top();
  stack.pop();

  auto selectOp = builder.create<SelectOp>(loc, cond, trueValue, falseValue);
  stack.push(selectOp.getResult());
}

void convertOperation(Operation *op, OpBuilder &builder, ValueStack &stack,
                      bool ignoreIfBlocks) {
  // Don't convert operations inside if statements, otherwise the post-order
  // traversal will end up outlining the then and else block
  if (ignoreIfBlocks && op->getParentOfType<iir::IfOp>())
    return;

  if (auto literalAccessExprOp = dyn_cast<iir::LiteralAccessExprOp>(op))
    convert(literalAccessExprOp, builder, stack);
  else if (auto fieldAccessExprOp = dyn_cast<iir::FieldAccessExprOp>(op))
    convert(fieldAccessExprOp, builder, stack);
  else if (auto varAccessExprOp = dyn_cast<iir::VarAccessExprOp>(op))
    convert(varAccessExprOp, builder, stack);
  else if (auto binaryOperatorOp = dyn_cast<iir::BinaryOperatorOp>(op))
    convert(binaryOperatorOp, builder, stack);
  else if (auto unaryOperatorOp = dyn_cast<iir::UnaryOperatorOp>(op))
    convert(unaryOperatorOp, builder, stack);
  else if (auto assignmentExprOp = dyn_cast<iir::AssignmentExprOp>(op))
    convert(assignmentExprOp, builder, stack);
  else if (auto varDeclStatementOp = dyn_cast<iir::VarDeclStatementOp>(op))
    convert(varDeclStatementOp, builder, stack);
  else if (auto functionCallOp = dyn_cast<iir::FunCallOp>(op))
    convert(functionCallOp, builder, stack);
  else if (auto ifOp = dyn_cast<iir::IfOp>(op))
    convert(ifOp, builder, stack);
  else if (auto ternaryOperatorOp = dyn_cast<iir::TernaryOperatorOp>(op))
    convert(ternaryOperatorOp, builder, stack);
}

// TODO: Tablegen this
template <typename IIRTerminatorOp, typename StencilTerminatorOp>
class TerminatorOpConversion final : public ConversionPattern {
public:
  explicit TerminatorOpConversion(MLIRContext *context)
      : ConversionPattern(IIRTerminatorOp::getOperationName(), 1, context) {}

  PatternMatchResult
  matchAndRewrite(Operation *operation, ArrayRef<Value *> operands,
                  ConversionPatternRewriter &rewriter) const override {
    rewriter.replaceOpWithNewOp<StencilTerminatorOp>(operation);

    return matchSuccess();
  }
};

class IIROpConversion final : public ConversionPattern {
public:
  explicit IIROpConversion(MLIRContext *context)
      : ConversionPattern(iir::IIROp::getOperationName(), 1, context) {}

  PatternMatchResult
  matchAndRewrite(Operation *operation, ArrayRef<Value *> operands,
                  ConversionPatternRewriter &rewriter) const override {
    auto rootIIROp = cast<iir::IIROp>(operation);

    SmallVector<StringRef, 3> apiFieldNames;
    auto apiFieldIDsAttr = operation->getAttr("apiFieldIDs");
    if (!apiFieldIDsAttr) {
      operation->emitError("expected 'apiFieldIDs' attribute");
      return matchFailure();
    }
    auto apiFieldIDs = apiFieldIDsAttr.cast<ArrayAttr>();
    auto accessIDToNameAttr = operation->getAttr("accessIDToName");
    if (!accessIDToNameAttr) {
      operation->emitError("expected 'accessIDToName' attribute");
      return matchFailure();
    }
    auto accessIDToName = accessIDToNameAttr.cast<DictionaryAttr>();
    for (const auto &apiFieldIDAttr : apiFieldIDs) {
      auto apiFieldID = apiFieldIDAttr.cast<IntegerAttr>().getInt();
      auto apiFieldNameAttr =
          accessIDToName.get(std::string("_") + std::to_string(apiFieldID));
      if (!apiFieldNameAttr) {
        operation->emitError(
            Twine("unknown field ID", std::to_string(apiFieldID)));
        return matchFailure();
      }
      auto apiFieldName = apiFieldNameAttr.cast<StringAttr>().getValue();
      apiFieldNames.push_back(apiFieldName);
    }

    auto stencilNameAttr = rootIIROp.getStencilName();
    if (!stencilNameAttr) {
      operation->emitError("expected stencil name attriubute");
      return matchFailure();
    }
    auto iirOp = rewriter.create<stencil::IIROp>(operation->getLoc(),
                                                 stencilNameAttr.getValue());
    iirOp.getOperation()->getRegion(0).takeBody(operation->getRegion(0));

    OpBuilder builder(&iirOp.getBody().front());

    SmallVector<Type, 3> apiFieldTypes(
        apiFieldNames.size(), stencil::FieldType::get(rewriter.getF64Type()));

    iirOp.getOperation()->walk([&](iir::StencilOp stencilOp) {
      convert(stencilOp, builder, apiFieldTypes, apiFieldNames);
    });

    // Mark array variable accesses
    iirOp.getOperation()->walk(
        [&rewriter](iir::VarAccessExprOp varAccessExprOp) {
          if (varAccessExprOp.getIndex().getOperations().size() > 1)
            varAccessExprOp.setAttr("isArray", rewriter.getUnitAttr());
        });

    rewriter.replaceOp(operation, llvm::None);

    return matchSuccess();
  }
};

class StencilOpConversion final : public ConversionPattern {
public:
  explicit StencilOpConversion(MLIRContext *context)
      : ConversionPattern(iir::StencilOp::getOperationName(), 1, context) {}

  PatternMatchResult
  matchAndRewrite(Operation *operation, ArrayRef<Value *> operands,
                  ConversionPatternRewriter &rewriter) const override {
    rewriter.replaceOp(operation, llvm::None);

    return matchSuccess();
  }
};

class MultiStageOpConversion final : public ConversionPattern {
public:
  explicit MultiStageOpConversion(MLIRContext *context)
      : ConversionPattern(iir::MultiStageOp::getOperationName(), 1, context) {}

  PatternMatchResult
  matchAndRewrite(Operation *operation, ArrayRef<Value *> operands,
                  ConversionPatternRewriter &rewriter) const override {
    auto loopOrder = cast<iir::MultiStageOp>(operation).loopOrder();
    auto multiStageOp = rewriter.create<stencil::MultiStageOp>(
        operation->getLoc(),
        *stencil::symbolizeLoopOrder(static_cast<int64_t>(loopOrder)));
    multiStageOp.getOperation()->getRegion(0).takeBody(operation->getRegion(0));

    rewriter.replaceOp(operation, llvm::None);

    return matchSuccess();
  }
};

class StageOpConversion final : public ConversionPattern {
public:
  explicit StageOpConversion(MLIRContext *context)
      : ConversionPattern(iir::StageOp::getOperationName(), 1, context) {}

  PatternMatchResult
  matchAndRewrite(Operation *operation, ArrayRef<Value *> operands,
                  ConversionPatternRewriter &rewriter) const override {
    auto stageOp = rewriter.create<stencil::StageOp>(operation->getLoc());
    stageOp.getOperation()->getRegion(0).takeBody(operation->getRegion(0));

    rewriter.replaceOp(operation, llvm::None);

    return matchSuccess();
  }
};

class DoMethodOpConversion final : public ConversionPattern {
public:
  explicit DoMethodOpConversion(MLIRContext *context)
      : ConversionPattern(iir::DoMethodOp::getOperationName(), 1, context) {}

  PatternMatchResult
  matchAndRewrite(Operation *operation, ArrayRef<Value *> operands,
                  ConversionPatternRewriter &rewriter) const override {
    auto iirDoMethodOp = cast<iir::DoMethodOp>(operation);
    iir::Interval interval = iirDoMethodOp.getInterval();
    auto doMethodOp = rewriter.create<stencil::DoMethodOp>(
        operation->getLoc(), stencil::Interval(interval));
    doMethodOp.getOperation()->getRegion(0).takeBody(operation->getRegion(0));

    OpBuilder builder(&doMethodOp.getBody().front());
    ValueStack stack;

    doMethodOp.walk(
        [&](Operation *op) { convertOperation(op, builder, stack); });

    rewriter.replaceOp(operation, llvm::None);

    return matchSuccess();
  }
};

class BlockStatementOpConversion final : public ConversionPattern {
public:
  explicit BlockStatementOpConversion(MLIRContext *context)
      : ConversionPattern(iir::BlockStatementOp::getOperationName(), 1,
                          context) {}

  PatternMatchResult
  matchAndRewrite(Operation *operation, ArrayRef<Value *> operands,
                  ConversionPatternRewriter &rewriter) const override {
    rewriter.replaceOp(operation, llvm::None);

    return matchSuccess();
  }
};

} // namespace

namespace {
/// Import the IIR Ops to Stencil Patterns.
#include "IIRToStencil.cpp.inc"
} // namespace

namespace mlir {
void populateIIRToStencilPatterns(MLIRContext *context,
                                  OwningRewritePatternList &patterns) {
  populateWithGenerated(context, &patterns);
  patterns.insert<IIROpConversion, StencilOpConversion, MultiStageOpConversion,
                  StageOpConversion, DoMethodOpConversion,
                  BlockStatementOpConversion>(context);
  patterns.insert<
      TerminatorOpConversion<iir::IIREndOp, stencil::IIREndOp>,
      TerminatorOpConversion<iir::StencilEndOp, stencil::StencilEndOp>,
      TerminatorOpConversion<iir::MultiStageEndOp, stencil::MultiStageEndOp>,
      TerminatorOpConversion<iir::StageEndOp, stencil::StageEndOp>,
      TerminatorOpConversion<iir::DoMethodEndOp, stencil::DoMethodEndOp>,
      TerminatorOpConversion<iir::IfEndOp, loop::TerminatorOp>>(context);
}
} // namespace mlir
