set(LLVM_TARGET_DEFINITIONS IIRToStencil.td)
mlir_tablegen(IIRToStencil.cpp.inc -gen-rewriters)
add_public_tablegen_target(MLIRIIRToStencilIncGen)

add_llvm_library(MLIRStencilConversion
        ConvertIIRToStencil.cpp
        ConvertIIRToStencilPass.cpp

        ADDITIONAL_HEADER_DIRS
        ${MLIR_MAIN_INCLUDE_DIR}/mlir/Dialect/Stencil/Stencil
        ${MLIR_MAIN_INCLUDE_DIR}/mlir/IR
        )

add_dependencies(MLIRStencilConversion
        MLIRIIRToStencilIncGen)

target_link_libraries(MLIRStencilConversion
        MLIRIR
        MLIRPass
        MLIRStencil
        MLIRStencilIIR
        MLIRSupport
        MLIRTransformUtils
        )
