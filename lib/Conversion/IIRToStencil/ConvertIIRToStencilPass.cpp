//===- ConvertIIRToStencilPass.cpp - Convert IIR Ops to Stencil Ops -------===//
//
// Copyright 2019 Jean-Michel Gorius
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// =============================================================================
//
// This file implements a pass to convert MLIR IIR ops into the Stencil
// ops.
//
//===----------------------------------------------------------------------===//

#include "mlir/Conversion/IIRToStencil/ConvertIIRToStencil.h"
#include "mlir/Dialect/Stencil/Stencil/Passes.h"
#include "mlir/Dialect/Stencil/Stencil/StencilDialect.h"

using namespace mlir;

namespace {
/// A pass converting IIR operations into the Stencil dialect.
class ConvertIIRToStencilPass : public ModulePass<ConvertIIRToStencilPass> {
  void runOnModule() override;
};
} // namespace

void ConvertIIRToStencilPass::runOnModule() {
  OwningRewritePatternList patterns;
  auto module = getModule();

  // Root IIR op + terminator
  assert(module.getBody()->getOperations().size() == 2 &&
         "module should contain only one root IIR operation");

  populateIIRToStencilPatterns(module.getContext(), patterns);
  ConversionTarget target(*(module.getContext()));
  target.addLegalDialect<stencil::StencilDialect>();

  // FIXME: Switch to full conversion
  if (failed(applyPartialConversion(module, target, patterns))) {
    return signalPassFailure();
  }
}

std::unique_ptr<OpPassBase<ModuleOp>>
mlir::stencil::createConvertIIRToStencilPass() {
  return std::make_unique<ConvertIIRToStencilPass>();
}

static PassRegistration<ConvertIIRToStencilPass>
    pass("convert-iir-to-stencil", "Convert IIR Ops to Stencil dialect");
