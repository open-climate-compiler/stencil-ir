#include "mlir/Dialect/Stencil/IIR/IIRAttributes.h"
#include "llvm/Support/raw_ostream.h"

using namespace mlir;
using namespace mlir::iir;

//===----------------------------------------------------------------------===//
// IntervalAttr
//===----------------------------------------------------------------------===//

struct mlir::iir::detail::IntervalAttributeStorage : public AttributeStorage {
  using KeyTy = Interval;

  explicit IntervalAttributeStorage(Interval value)
      : AttributeStorage(), value(value) {}

  /// Key equality function.
  bool operator==(const KeyTy &key) const { return key == value; }

  /// Construct a new storage instance.
  static IntervalAttributeStorage *
  construct(AttributeStorageAllocator &allocator, KeyTy key) {
    return new (allocator.allocate<IntervalAttributeStorage>())
        IntervalAttributeStorage(key);
  }

  Interval value;
};

IntervalAttr IntervalAttr::get(Interval value, MLIRContext *context) {
  return Base::get(context, AttrKind::Interval, value);
}

Interval IntervalAttr::getValue() const { return getImpl()->value; }

//===----------------------------------------------------------------------===//
// CacheAttr
//===----------------------------------------------------------------------===//

struct mlir::iir::detail::CacheAttributeStorage : public AttributeStorage {
  using KeyTy = Cache;

  explicit CacheAttributeStorage(Cache value)
      : AttributeStorage(), value(value) {}

  /// Key equality function.
  bool operator==(const KeyTy &key) const { return key == value; }

  /// Construct a new storage instance.
  static CacheAttributeStorage *construct(AttributeStorageAllocator &allocator,
                                          KeyTy key) {
    return new (allocator.allocate<CacheAttributeStorage>())
        CacheAttributeStorage(key);
  }

  Cache value;
};

CacheAttr CacheAttr::get(Cache value, MLIRContext *context) {
  return Base::get(context, AttrKind::Cache, value);
}

Cache CacheAttr::getValue() const { return getImpl()->value; }
