//===- IIRDialect.cpp - MLIR stencil IIR dialect --------------------------===//
//
// Copyright 2019 Jean-Michel Gorius
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// =============================================================================
//
// This file defines the stencil IIR dialect in MLIR.
//
//===----------------------------------------------------------------------===//

#include "mlir/Dialect/Stencil/IIR/IIRDialect.h"
#include "mlir/Dialect/Stencil/IIR/IIRAttributes.h"
#include "mlir/Dialect/Stencil/IIR/IIROps.h"
#include "mlir/Dialect/Stencil/IIR/IIRTypes.h"
#include "mlir/IR/Builders.h"
#include "mlir/IR/DialectImplementation.h"

using namespace mlir;
using namespace mlir::iir;

//===----------------------------------------------------------------------===//
// Stencil IIR Dialect
//===----------------------------------------------------------------------===//

IIRDialect::IIRDialect(mlir::MLIRContext *context)
    : Dialect(getDialectNamespace(), context) {
  addAttributes<IntervalAttr, CacheAttr>();

  addOperations<
#define GET_OP_LIST
#include "mlir/Dialect/Stencil/IIR/IIROps.cpp.inc"
      >();

  allowsUnknownOperations();
}

//===----------------------------------------------------------------------===//
// Attribute Parsing
//===----------------------------------------------------------------------===//

Attribute IIRDialect::parseAttribute(DialectAsmParser &parser,
                                     Type type) const {
  MLIRContext *context = getContext();

  auto parseInterval = [&](Interval &interval) {
    return parser.parseInteger(interval.lowerLevel) || parser.parseComma() ||
           parser.parseInteger(interval.lowerOffset) || parser.parseComma() ||
           parser.parseInteger(interval.upperLevel) || parser.parseComma() ||
           parser.parseInteger(interval.upperOffset);
  };

  StringRef attrName;
  if (parser.parseKeyword(&attrName))
    return Attribute();

  if (attrName == "interval") {
    // Mirror interval printing
    Interval interval{};
    if (parser.parseLess() || parseInterval(interval) || parser.parseGreater())
      return Attribute();
    return IntervalAttr::get(interval, context);
  } else if (attrName == "cache") {
    // Mirror cache printing
    Cache cache{};
    Interval interval;
    Interval enclosingAccessInterval;
    Window window;
    if (parser.parseLess() || parser.parseKeyword("accessID") ||
        parser.parseColon() || parser.parseInteger(cache.accessID) ||
        parser.parseComma() || parser.parseKeyword("type") ||
        parser.parseColon())
      return Attribute();
    auto cacheTypeLoc = parser.getNameLoc();
    StringAttr cacheTypeAttr;
    if (parser.parseAttribute(cacheTypeAttr) || parser.parseComma() ||
        parser.parseKeyword("policy") || parser.parseColon())
      return Attribute();
    auto cachePolicyLoc = parser.getNameLoc();
    StringAttr cachePolicyAttr;
    if (parser.parseAttribute(cachePolicyAttr))
      return Attribute();
    if (succeeded(parser.parseOptionalComma()) &&
        succeeded(parser.parseOptionalKeyword("interval"))) {
      if (parser.parseColon() || parser.parseLSquare() ||
          parseInterval(interval) || parser.parseRSquare())
        return Attribute();
      cache.interval = interval;
    }
    if (succeeded(parser.parseOptionalComma()) &&
        succeeded(parser.parseOptionalKeyword("enclosingAccessInterval"))) {
      if (parser.parseColon() || parser.parseLSquare() ||
          parseInterval(enclosingAccessInterval) || parser.parseRSquare())
        return Attribute();
      cache.enclosingAccessInterval = enclosingAccessInterval;
    }
    if (succeeded(parser.parseOptionalComma()) &&
        succeeded(parser.parseOptionalKeyword("window"))) {
      if (parser.parseColon() || parser.parseLSquare() ||
          parser.parseInteger(window.minus) || parser.parseComma() ||
          parser.parseInteger(window.plus) || parser.parseRSquare())
        return Attribute();
      cache.window = window;
    }
    if (parser.parseGreater())
      return Attribute();

    auto cacheType = symbolizeCacheType(cacheTypeAttr.getValue());
    if (!cacheType) {
      parser.emitError(cacheTypeLoc,
                       Twine("unknown cache type ", cacheTypeAttr.getValue()));
      return Attribute();
    }
    auto cachePolicy = symbolizeCachePolicy(cachePolicyAttr.getValue());
    if (!cachePolicy) {
      parser.emitError(cachePolicyLoc, Twine("unknown cache policy ",
                                             cachePolicyAttr.getValue()));
      return Attribute();
    }
    cache.type = cacheType.getValue();
    cache.policy = cachePolicy.getValue();
    return CacheAttr::get(cache, context);
  }

  parser.emitError(parser.getNameLoc(), "unknown IIR attribute: ")
      << parser.getFullSymbolSpec();
  return Attribute();
}

//===----------------------------------------------------------------------===//
// Attribute Printing
//===----------------------------------------------------------------------===//

void IIRDialect::printAttribute(Attribute attr,
                                DialectAsmPrinter &printer) const {
  switch (attr.getKind()) {
  case AttrKind::Interval: {
    Interval interval = attr.cast<IntervalAttr>().getValue();
    printer << IntervalAttr::getAttributeName() << "<" << interval << ">";
    break;
  }
  case AttrKind::Cache: {
    Cache cache = attr.cast<CacheAttr>().getValue();
    printer << CacheAttr::getAttributeName() << "<" << cache << ">";
    break;
  }
  default:
    llvm_unreachable("unhandled IIR attribute");
  }
}

//===----------------------------------------------------------------------===//
// Type Parsing
//===----------------------------------------------------------------------===//

Type IIRDialect::parseType(DialectAsmParser &parser) const {
  parser.emitError(parser.getNameLoc(), "unknown IIR type: ")
      << parser.getFullSymbolSpec();
  return Type();
}

//===----------------------------------------------------------------------===//
// Type Printing
//===----------------------------------------------------------------------===//

void IIRDialect::printType(Type type, DialectAsmPrinter &os) const {
  switch (type.getKind()) {
  default:
    llvm_unreachable("unhandled IIR type");
  }
}
