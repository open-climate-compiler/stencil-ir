#include "mlir/Dialect/Stencil/IIR/Interval.h"

namespace mlir {
namespace iir {

const Interval Interval::Invalid{-1, -1, -1, -1};

bool operator==(const Interval &lhs, const Interval &rhs) {
  return lhs.lowerLevel == rhs.lowerLevel &&
         lhs.lowerOffset == rhs.lowerOffset &&
         lhs.upperLevel == rhs.upperLevel && lhs.upperOffset == rhs.upperOffset;
}

llvm::raw_ostream &operator<<(llvm::raw_ostream &os, Interval interval) {
  os << interval.lowerLevel << ", " << interval.lowerOffset << ", "
     << interval.upperLevel << ", " << interval.upperOffset;
  return os;
}

} // namespace iir
} // namespace mlir
