#include "mlir/Dialect/Stencil/IIR/Cache.h"

namespace mlir {
namespace iir {

const Window Window::Invalid{-1, -1};

bool operator==(const Window &lhs, const Window &rhs) {
  return lhs.minus == rhs.minus && lhs.plus == rhs.plus;
}

llvm::raw_ostream &operator<<(llvm::raw_ostream &os, Window window) {
  os << '[' << window.minus << ", " << window.plus << ']';
  return os;
}

bool operator==(const Cache &lhs, const Cache &rhs) {
  return lhs.accessID == rhs.accessID && lhs.window == rhs.window &&
         lhs.type == rhs.type && lhs.policy == rhs.policy &&
         lhs.interval == rhs.interval &&
         lhs.enclosingAccessInterval == rhs.enclosingAccessInterval;
}

llvm::raw_ostream &operator<<(llvm::raw_ostream &os, Cache cache) {
  os << "accessID: " << cache.accessID << ", type: \""
     << stringifyCacheType(cache.type) << "\""
     << ", policy: \"" << stringifyCachePolicy(cache.policy) << "\"";
  if (cache.interval)
    os << ", interval: [" << cache.interval.getValue() << "]";
  if (cache.enclosingAccessInterval)
    os << ", enclosingAccessInterval: ["
       << cache.enclosingAccessInterval.getValue() << "]";
  if (cache.window)
    os << ", window: " << cache.window.getValue();
  return os;
}

} // namespace iir
} // namespace mlir
