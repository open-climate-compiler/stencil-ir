#include "mlir/Dialect/Stencil/Stencil/StencilAttributes.h"
#include "mlir/Dialect/Stencil/Stencil/StencilOps.h"
#include "mlir/IR/Builders.h"
#include "mlir/Pass/Pass.h"
#include "mlir/Pass/PassRegistry.h"
#include <algorithm>
#include <iterator>

namespace mlir {
namespace stencil {
namespace {
struct IntervalPartitioner : public ModulePass<IntervalPartitioner> {
  void runOnModule() override;
};

std::vector<Interval> computePartition(std::vector<Interval> &intervals) {
  std::vector<std::pair<int64_t, int64_t>> levels;
  for (auto &interval : intervals) {
    levels.push_back({interval.lowerLevel, interval.lowerOffset});
    levels.push_back({interval.upperLevel, interval.upperOffset + 1});
  }
  std::sort(levels.begin(), levels.end());
  auto new_end = std::unique(levels.begin(), levels.end());

  std::vector<Interval> result;
  auto dist = std::distance(levels.begin(), new_end);
  for (decltype(dist) i = 0; i < dist - 1; ++i) {
    result.push_back({levels[i].first, levels[i].second, //
                      levels[i + 1].first, levels[i + 1].second - 1});
  }
  return result;
} // namespace

std::vector<Interval>
computePartitionOfIntervals(std::vector<Interval> &intervals) {
  // compute the partition of the intervals
  auto partitionedIntervals = computePartition(intervals);
  // TODO maybe
  //   if (getLoopOrder() == iir::LoopOrderKind::Backward)
  //     std::reverse(partitionIntervals.begin(), partitionIntervals.end());
  return partitionedIntervals;
}

bool contains(Interval interval, Interval in) {
  using lev = std::pair<int64_t, int64_t>;
  lev begin = {interval.lowerLevel, interval.lowerOffset};
  lev end = {interval.upperLevel, interval.upperOffset};
  return begin >= lev{in.lowerLevel, in.lowerOffset} &&
         end <= lev{in.upperLevel, in.upperOffset};
}

void IntervalPartitioner::runOnModule() {
  auto module = getModule();

  module.walk([](MultiStageOp multistage) {
    std::vector<Interval> intervals;
    multistage.walk([&intervals](DoMethodOp doOp) {
      auto interval = doOp.getInterval();
      intervals.push_back(interval);
      llvm::outs() << interval << "\n";
    });
    auto new_intervals = computePartitionOfIntervals(intervals);

    multistage.walk([&new_intervals](DoMethodOp doOp) {
      OpBuilder builder{doOp};
      auto interval = doOp.getInterval();
      for (auto const &new_interval : new_intervals) {
        if (contains(new_interval, interval)) {
          auto new_do_method = builder.clone(*doOp.getOperation());
          cast<DoMethodOp>(new_do_method).setInterval(new_interval);
        }
      }
      doOp.erase();
    });
  });
}

static PassRegistration<IntervalPartitioner> pass("stencil-partition-intervals",
                                                  "Partitions intervals");

} // namespace
} // namespace stencil
} // namespace mlir
