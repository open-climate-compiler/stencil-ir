//===- StencilOps.cpp - MLIR Stencil operations ---------------------------===//
//
// Copyright 2019 Jean-Michel Gorius
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// =============================================================================
//
// This file defines the operations in the Stencil dialect.
//
//===----------------------------------------------------------------------===//

#include "mlir/Dialect/Stencil/Stencil/StencilOps.h"
#include "mlir/Dialect/Stencil/Stencil/StencilAttributes.h"
#include "mlir/Dialect/Stencil/Stencil/StencilDialect.h"
#include "mlir/Dialect/Stencil/Stencil/StencilTypes.h"
#include "mlir/IR/Attributes.h"
#include "mlir/IR/Builders.h"
#include "mlir/IR/Function.h"
#include "mlir/IR/OpImplementation.h"

using namespace mlir;

//===----------------------------------------------------------------------===//
// Common utility functions
//===----------------------------------------------------------------------===//

// Parses an op that has no inputs and no outputs.
static ParseResult parseNoIOOp(OpAsmParser &parser, OperationState &state) {
  if (parser.parseOptionalAttrDict(state.attributes))
    return failure();
  return success();
}

// Prints an op that has no inputs and no outputs.
static void printNoIOOp(Operation *op, OpAsmPrinter &printer) {
  printer << op->getName();
  printer.printOptionalAttrDict(op->getAttrs());
}

static void printRegionOp(Operation *op, OpAsmPrinter &printer) {
  printer << op->getName();

  for (auto &region : op->getRegions()) {
    printer.printRegion(region, false, false);
  }

  if (!op->getAttrs().empty()) {
    printer << " attributes";
    printer.printOptionalAttrDict(op->getAttrs());
  }
}

template <typename StructureOp>
static ParseResult parseRegionOp(OpAsmParser &parser, OperationState &state,
                                 unsigned int nRegions = 1) {
  llvm::SmallVector<Region *, 2> regions;
  for (unsigned int i = 0; i < nRegions; ++i)
    regions.push_back(state.addRegion());

  for (auto &region : regions) {
    if (parser.parseRegion(*region, /*arguments=*/{}, /*argTypes=*/{}))
      return failure();
    StructureOp::ensureTerminator(*region, parser.getBuilder(), state.location);
  }

  if (succeeded(parser.parseOptionalKeyword("attributes"))) {
    if (parser.parseOptionalAttrDict(state.attributes))
      return failure();
  }

  return success();
}

//===----------------------------------------------------------------------===//
// stencil.field
//===----------------------------------------------------------------------===//

void stencil::FieldOp::build(Builder *builder, OperationState &state,
                             StringRef name) {
  // TODO: Handle other element types
  auto nameAttr =
      StringAttr::get(name, stencil::FieldType::get(builder->getF64Type()));
  state.addAttribute(stencil::FieldOp::getNameAttrName(), nameAttr);
  state.addTypes(nameAttr.getType());
}

static ParseResult parseFieldOp(OpAsmParser &parser, OperationState &state) {
  StringAttr fieldName;
  if (parser.parseAttribute(fieldName, stencil::FieldOp::getNameAttrName(),
                            state.attributes))
    return failure();

  return parser.addTypesToList(fieldName.getType(), state.types);
}

static void print(stencil::FieldOp fieldOp, OpAsmPrinter &printer) {
  printer << stencil::FieldOp::getOperationName() << ' ';
  printer.printAttribute(fieldOp.getAttr(stencil::FieldOp::getNameAttrName()));
}

static LogicalResult verify(stencil::FieldOp fieldOp) { return success(); }

//===----------------------------------------------------------------------===//
// stencil.field_access
//===----------------------------------------------------------------------===//

void stencil::FieldAccessOp::build(Builder *builder, OperationState &state,
                                   Value *field, ArrayRef<int64_t> offset) {
  assert(offset.size() == 3 && "offset expects three elements");
  state.addOperands(field);
  state.addAttribute(stencil::FieldAccessOp::getOffsetAttrName(),
                     builder->getI64ArrayAttr(offset));
  state.addTypes(stencil::PointerType::get(
      field->getType().cast<stencil::FieldType>().getElementType()));
}

void stencil::FieldAccessOp::build(Builder *builder, OperationState &state,
                                   Value *field, ArrayAttr offsetAttr) {
  assert(offsetAttr.size() == 3 && "offset attribute expects three elements");
  state.addOperands(field);
  state.addAttribute(stencil::FieldAccessOp::getOffsetAttrName(), offsetAttr);
  state.addTypes(stencil::PointerType::get(
      field->getType().cast<stencil::FieldType>().getElementType()));
}

static ParseResult parseFieldAccessOp(OpAsmParser &parser,
                                      OperationState &state) {
  OpAsmParser::OperandType field;
  ArrayAttr offset;
  Type pointerType;
  if (parser.parseOperand(field) ||
      parser.parseAttribute(offset, stencil::FieldAccessOp::getOffsetAttrName(),
                            state.attributes) ||
      parser.parseColonType(pointerType) ||
      parser.resolveOperand(
          field,
          stencil::FieldType::get(
              pointerType.cast<stencil::PointerType>().getPointeeType()),
          state.operands) ||
      parser.addTypesToList(pointerType, state.types))
    return failure();

  return success();
}

static void print(stencil::FieldAccessOp fieldAccessOp, OpAsmPrinter &printer) {
  printer << stencil::FieldAccessOp::getOperationName() << ' '
          << *fieldAccessOp.field() << ' ';
  printer.printAttribute(
      fieldAccessOp.getAttr(stencil::FieldAccessOp::getOffsetAttrName()));
  printer << " : " << fieldAccessOp.res()->getType();
}

// TODO: Check consistency
static LogicalResult verify(stencil::FieldAccessOp fieldAccessOp) {
  return success();
}

//===----------------------------------------------------------------------===//
// stencil.var
//===----------------------------------------------------------------------===//

void stencil::VarOp::build(Builder *builder, OperationState &state,
                           StringRef name) {
  // TODO: Handle other element types
  auto nameAttr =
      StringAttr::get(name, stencil::VarType::get(builder->getF64Type()));
  state.addAttribute(stencil::VarOp::getNameAttrName(), nameAttr);
  state.addTypes(nameAttr.getType());
}

static ParseResult parseVarOp(OpAsmParser &parser, OperationState &state) {
  StringAttr varName;
  if (parser.parseAttribute(varName, stencil::VarOp::getNameAttrName(),
                            state.attributes))
    return failure();

  return parser.addTypesToList(varName.getType(), state.types);
}

static void print(stencil::VarOp varOp, OpAsmPrinter &printer) {
  printer << stencil::VarOp::getOperationName() << ' ';
  printer.printAttribute(varOp.getAttr(stencil::VarOp::getNameAttrName()));
}

static LogicalResult verify(stencil::VarOp varOp) { return success(); }

//===----------------------------------------------------------------------===//
// stencil.var_access
//===----------------------------------------------------------------------===//

void stencil::VarAccessOp::build(Builder *builder, OperationState &state,
                                 Value *var) {
  state.addOperands(var);
  state.addTypes(stencil::PointerType::get(
      var->getType().cast<stencil::VarType>().getContentType()));
}

static ParseResult parseVarAccessOp(OpAsmParser &parser,
                                    OperationState &state) {
  OpAsmParser::OperandType var;
  Type pointerType;
  if (parser.parseOperand(var) || parser.parseColonType(pointerType) ||
      parser.resolveOperand(
          var,
          stencil::VarType::get(
              pointerType.cast<stencil::PointerType>().getPointeeType()),
          state.operands) ||
      parser.addTypesToList(pointerType, state.types))
    return failure();

  return success();
}

static void print(stencil::VarAccessOp varAccessOp, OpAsmPrinter &printer) {
  printer << stencil::VarAccessOp::getOperationName() << ' '
          << *varAccessOp.var() << " : "
          << varAccessOp.res()->getType().cast<stencil::PointerType>();
}

// TODO: Check consistency
static LogicalResult verify(stencil::VarAccessOp varAccessOp) {
  return success();
}

//===----------------------------------------------------------------------===//
// stencil.get_value
//===----------------------------------------------------------------------===//

void stencil::GetValueOp::build(Builder *builder, OperationState &state,
                                Value *ptr) {
  state.addOperands(ptr);
  state.addTypes(ptr->getType().cast<stencil::PointerType>().getPointeeType());
}

static ParseResult parseGetValueOp(OpAsmParser &parser, OperationState &state) {
  OpAsmParser::OperandType ptr;
  Type pointeeType;
  if (parser.parseOperand(ptr) || parser.parseColonType(pointeeType) ||
      parser.resolveOperand(ptr, stencil::PointerType::get(pointeeType),
                            state.operands) ||
      parser.addTypesToList(pointeeType, state.types))
    return failure();

  return success();
}

static void print(stencil::GetValueOp getValueOp, OpAsmPrinter &printer) {
  auto ptr = getValueOp.ptr();
  printer << stencil::GetValueOp::getOperationName() << ' ' << *ptr << " : "
          << ptr->getType().cast<stencil::PointerType>().getPointeeType();
}

static LogicalResult verify(stencil::GetValueOp getValueOp) {
  return success();
}

//===----------------------------------------------------------------------===//
// stencil.write
//===----------------------------------------------------------------------===//

void stencil::WriteOp::build(Builder *builder, OperationState &state,
                             Value *ptr, Value *value) {
  state.addOperands({ptr, value});
}

static ParseResult parseWriteOp(OpAsmParser &parser, OperationState &state) {
  OpAsmParser::OperandType target, value;
  Type valueType;
  if (parser.parseOperand(target) || parser.parseComma() ||
      parser.parseOperand(value) || parser.parseColonType(valueType) ||
      parser.resolveOperands({target, value},
                             {stencil::PointerType::get(valueType), valueType},
                             parser.getCurrentLocation(), state.operands))
    return failure();

  return success();
}

static void print(stencil::WriteOp writeOp, OpAsmPrinter &printer) {
  auto target = writeOp.target();
  auto value = writeOp.value();
  printer << stencil::WriteOp::getOperationName() << ' ' << *target << ", "
          << *value << " : " << value->getType();
}

static LogicalResult verify(stencil::WriteOp writeOp) {
  Type targetType =
      writeOp.target()->getType().cast<stencil::PointerType>().getPointeeType();
  Type valueType = writeOp.value()->getType();

  if (targetType != valueType)
    return writeOp.emitOpError("inconsistent target type '")
           << targetType << "' and value type '" << valueType << "'";

  return success();
}

//===----------------------------------------------------------------------===//
// stencil.iir
//===----------------------------------------------------------------------===//

void stencil::IIROp::build(Builder *builder, OperationState &state,
                           StringRef stencilName) {
  state.addAttribute(stencil::IIROp::getStencilNameAttrName(),
                     builder->getStringAttr(stencilName));
  ensureTerminator(*state.addRegion(), *builder, state.location);
}

//===----------------------------------------------------------------------===//
// stencil.stencil
//===----------------------------------------------------------------------===//

void stencil::StencilOp::build(
    Builder *builder, OperationState &state,
    ArrayRef<stencil::StencilAttribute> stencilAttributes) {
  for (const auto &attr : stencilAttributes) {
    switch (attr) {
    case stencil::StencilAttribute::MergeDoMethods:
      state.addAttribute(stencil::StencilOp::getMergeDoMethodsAttrName(),
                         builder->getUnitAttr());
      break;
    case stencil::StencilAttribute::MergeStages:
      state.addAttribute(stencil::StencilOp::getMergeStagesAttrName(),
                         builder->getUnitAttr());
      break;
    case stencil::StencilAttribute::MergeTemporaries:
      state.addAttribute(stencil::StencilOp::getMergeTemporariesAttrName(),
                         builder->getUnitAttr());
      break;
    case stencil::StencilAttribute::NoCodeGen:
      state.addAttribute(stencil::StencilOp::getNoCodeGenAttrName(),
                         builder->getUnitAttr());
      break;
    case stencil::StencilAttribute::UseKCaches:
      state.addAttribute(stencil::StencilOp::getUseKCachesAttrName(),
                         builder->getUnitAttr());
      break;
    }
  }

  ensureTerminator(*state.addRegion(), *builder, state.location);
}

static ParseResult parseStencilOp(OpAsmParser &parser, OperationState &state) {
  Region *bodyRegion = state.addRegion();
  SmallVector<OpAsmParser::OperandType, 3> arguments;
  SmallVector<Type, 3> argumentTypes;

  auto parseArgument = [&]() -> ParseResult {
    OpAsmParser::OperandType argument;
    Type argumentType;
    if (succeeded(parser.parseOptionalRegionArgument(argument)) &&
        !argument.name.empty()) {
      arguments.push_back(argument);

      if (parser.parseColonType(argumentType))
        return failure();
    } else {
      return failure();
    }

    argumentTypes.push_back(argumentType);
    return success();
  };

  if (parser.parseLParen())
    return failure();
  if (parser.parseOptionalRParen()) {
    do {
      if (parseArgument())
        return failure();
    } while (succeeded(parser.parseOptionalComma()));
    parser.parseRParen();
  }

  if (parser.parseRegion(*bodyRegion, arguments, argumentTypes))
    return failure();

  if (parser.parseOptionalAttrDict(state.attributes))
    return failure();

  stencil::StencilOp::ensureTerminator(*bodyRegion, parser.getBuilder(),
                                       state.location);

  return success();
}

static void print(stencil::StencilOp stencilOp, OpAsmPrinter &printer) {
  auto *op = stencilOp.getOperation();
  auto &body = stencilOp.getBody();

  printer << stencilOp.getOperationName() << '(';
  for (unsigned i = 0, e = body.getNumArguments(); i < e; ++i) {
    if (i > 0)
      printer << ", ";

    auto arg = body.getArgument(i);

    printer.printOperand(arg);
    printer << ": ";
    printer.printType(arg->getType());
  }
  printer << ')';

  printer.printRegion(op->getRegion(0), /*printEntryBlockArgs=*/false,
                      /*printBlockTerminators=*/false);
}

//===----------------------------------------------------------------------===//
// stencil.multi_stage
//===----------------------------------------------------------------------===//

void stencil::MultiStageOp::build(Builder *builder, OperationState &state,
                                  stencil::LoopOrder loopOrder) {
  state.addAttribute(
      stencil::MultiStageOp::getLoopOrderAttrName(),
      builder->getI64IntegerAttr(static_cast<int64_t>(loopOrder)));
  ensureTerminator(*state.addRegion(), *builder, state.location);
}

static ParseResult parseLoopOrder(stencil::LoopOrder &value,
                                  OpAsmParser &parser, OperationState &state) {
  Attribute attrVal;
  SmallVector<NamedAttribute, 1> attr;
  auto loc = parser.getCurrentLocation();
  if (parser.parseAttribute(attrVal, parser.getBuilder().getNoneType(),
                            stencil::MultiStageOp::getLoopOrderAttrName(),
                            attr)) {
    return failure();
  }
  if (!attrVal.isa<StringAttr>()) {
    return parser.emitError(loc, "expected '")
           << stencil::MultiStageOp::getLoopOrderAttrName()
           << "' attribute specified as string";
  }
  auto attrOptional =
      stencil::symbolizeLoopOrder(attrVal.cast<StringAttr>().getValue());
  if (!attrOptional) {
    return parser.emitError(loc, "invalid '")
           << stencil::MultiStageOp::getLoopOrderAttrName()
           << "' attribute specification: " << attrVal;
  }
  value = attrOptional.getValue();

  state.addAttribute(
      stencil::MultiStageOp::getLoopOrderAttrName(),
      parser.getBuilder().getI64IntegerAttr(static_cast<int64_t>(value)));

  return success();
}

static ParseResult parseMultiStageOp(OpAsmParser &parser,
                                     OperationState &state) {
  stencil::LoopOrder loopOrderAttr;
  if (parseLoopOrder(loopOrderAttr, parser, state))
    return failure();

  Region *body = state.addRegion();
  if (parser.parseRegion(*body, /*arguments=*/{}, /*argTypes=*/{}))
    return failure();

  if (parser.parseOptionalAttrDict(state.attributes))
    return failure();

  stencil::MultiStageOp::ensureTerminator(*body, parser.getBuilder(),
                                          state.location);

  return success();
}

static void print(stencil::MultiStageOp multiStageOp, OpAsmPrinter &printer) {
  auto *op = multiStageOp.getOperation();

  printer << multiStageOp.getOperationName() << " \""
          << stencil::stringifyLoopOrder(multiStageOp.loopOrder()) << "\"";

  printer.printRegion(op->getRegion(0), /*printEntryBlockArgs=*/false,
                      /*printBlockTerminators=*/false);

  if (op->getAttrs().size() > 1) {
    printer << " attributes";
    printer.printOptionalAttrDict(
        op->getAttrs(), {stencil::MultiStageOp::getLoopOrderAttrName()});
  }
}

//===----------------------------------------------------------------------===//
// stencil.stage
//===----------------------------------------------------------------------===//

void stencil::StageOp::build(Builder *builder, OperationState &state) {
  ensureTerminator(*state.addRegion(), *builder, state.location);
}

//===----------------------------------------------------------------------===//
// stencil.do_method
//===----------------------------------------------------------------------===//

void stencil::DoMethodOp::build(Builder *builder, OperationState &state,
                                stencil::Interval interval) {
  build(builder, state,
        stencil::IntervalAttr::get(interval, builder->getContext()));
}

void stencil::DoMethodOp::build(Builder *builder, OperationState &state,
                                stencil::IntervalAttr intervalAttr) {
  state.addAttribute(stencil::DoMethodOp::getIntervalAttrName(), intervalAttr);
  ensureTerminator(*state.addRegion(), *builder, state.location);
}

static ParseResult parseDoMethodOp(OpAsmParser &parser, OperationState &state) {
  IntegerAttr lowerLevel, lowerOffset, upperLevel, upperOffset;
  SmallVector<NamedAttribute, 4> intervalAttr;

  if (parser.parseLSquare() ||
      parser.parseAttribute(lowerLevel, "lowerLevel", intervalAttr) ||
      parser.parseComma() ||
      parser.parseAttribute(lowerOffset, "lowerOffset", intervalAttr) ||
      parser.parseComma() ||
      parser.parseAttribute(upperLevel, "upperLevel", intervalAttr) ||
      parser.parseComma() ||
      parser.parseAttribute(upperOffset, "upperOffset", intervalAttr) ||
      parser.parseRSquare())
    return failure();

  stencil::Interval interval = {lowerLevel.getInt(), lowerOffset.getInt(),
                                upperLevel.getInt(), upperOffset.getInt()};
  state.addAttribute(
      stencil::DoMethodOp::getIntervalAttrName(),
      stencil::IntervalAttr::get(interval, parser.getBuilder().getContext()));

  Region *body = state.addRegion();
  if (parser.parseRegion(*body, /*arguments=*/{}, /*argTypes=*/{}))
    return failure();

  if (parser.parseOptionalAttrDictWithKeyword(state.attributes))
    return failure();

  stencil::DoMethodOp::ensureTerminator(*body, parser.getBuilder(),
                                        state.location);

  return success();
}

static void print(stencil::DoMethodOp doMethodOp, OpAsmPrinter &printer) {
  auto *op = doMethodOp.getOperation();
  auto interval = doMethodOp.getInterval();
  printer << doMethodOp.getOperationName() << " [" << interval << "]";

  printer.printRegion(op->getRegion(0), /*printEntryBlockArgs=*/false,
                      /*printBlockTerminators=*/false);

  if (op->getAttrs().size() > 1) {
    printer << " attributes";
    printer.printOptionalAttrDict(op->getAttrs(),
                                  {stencil::DoMethodOp::getIntervalAttrName()});
  }
}

//===----------------------------------------------------------------------===//
// stencil.sqrtf
//===----------------------------------------------------------------------===//

void stencil::SqrtfOp::build(Builder *builder, OperationState &state,
                             Value *value) {
  state.addOperands(value);
  state.addTypes(value->getType());
}

static ParseResult parseSqrtfOp(OpAsmParser &parser, OperationState &state) {
  OpAsmParser::OperandType value;
  Type type;
  if (parser.parseOperand(value) || parser.parseColonType(type) ||
      parser.resolveOperand(value, type, state.operands) ||
      parser.addTypesToList(type, state.types))
    return failure();

  return success();
}

static void print(stencil::SqrtfOp sqrtfOp, OpAsmPrinter &printer) {
  auto value = sqrtfOp.value();
  printer << stencil::SqrtfOp::getOperationName() << ' ' << *value << " : "
          << value->getType();
}

static LogicalResult verify(stencil::SqrtfOp sqrtfOp) { return success(); }

//===----------------------------------------------------------------------===//
// stencil.fabs
//===----------------------------------------------------------------------===//

void stencil::FabsOp::build(Builder *builder, OperationState &state,
                            Value *value) {
  state.addOperands(value);
  state.addTypes(value->getType());
}

static ParseResult parseFabsOp(OpAsmParser &parser, OperationState &state) {
  OpAsmParser::OperandType value;
  Type type;
  if (parser.parseOperand(value) || parser.parseColonType(type) ||
      parser.resolveOperand(value, type, state.operands) ||
      parser.addTypesToList(type, state.types))
    return failure();

  return success();
}

static void print(stencil::FabsOp fabsOp, OpAsmPrinter &printer) {
  auto value = fabsOp.value();
  printer << stencil::FabsOp::getOperationName() << ' ' << *value << " : "
          << value->getType();
}

static LogicalResult verify(stencil::FabsOp fabsOp) { return success(); }

//===----------------------------------------------------------------------===//
// stencil.exp
//===----------------------------------------------------------------------===//

void stencil::ExpOp::build(Builder *builder, OperationState &state,
                           Value *value) {
  state.addOperands(value);
  state.addTypes(value->getType());
}

static ParseResult parseExpOp(OpAsmParser &parser, OperationState &state) {
  OpAsmParser::OperandType value;
  Type type;
  if (parser.parseOperand(value) || parser.parseColonType(type) ||
      parser.resolveOperand(value, type, state.operands) ||
      parser.addTypesToList(type, state.types))
    return failure();

  return success();
}

static void print(stencil::ExpOp expOp, OpAsmPrinter &printer) {
  auto value = expOp.value();
  printer << stencil::ExpOp::getOperationName() << ' ' << *value << " : "
          << value->getType();
}

static LogicalResult verify(stencil::ExpOp expOp) { return success(); }

//===----------------------------------------------------------------------===//
// stencil.pow
//===----------------------------------------------------------------------===//

void stencil::PowOp::build(Builder *builder, OperationState &state,
                           Value *value, Value *exponent) {
  state.addOperands(llvm::makeArrayRef({value, exponent}));
  state.addTypes(value->getType());
}

static ParseResult parsePowOp(OpAsmParser &parser, OperationState &state) {
  SmallVector<OpAsmParser::OperandType, 2> operands;
  Type type;
  if (parser.parseOperandList(operands, 2, OpAsmParser::Delimiter::None) ||
      parser.parseColonType(type) ||
      parser.resolveOperands(operands, type, state.operands) ||
      parser.addTypesToList(type, state.types))
    return failure();

  return success();
}

static void print(stencil::PowOp powOp, OpAsmPrinter &printer) {
  auto value = powOp.value();
  auto exponent = powOp.exponent();
  printer << stencil::PowOp::getOperationName() << ' ' << *value << ", "
          << *exponent << " : " << value->getType();
}

static LogicalResult verify(stencil::PowOp powOp) { return success(); }

namespace mlir {
namespace stencil {
#define GET_OP_CLASSES
#include "mlir/Dialect/Stencil/Stencil/StencilOps.cpp.inc"
} // namespace stencil
} // namespace mlir
