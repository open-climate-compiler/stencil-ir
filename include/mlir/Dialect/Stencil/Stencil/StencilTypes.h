//===- IIRTypes.h - MLIR Stencil Types ---------------------------*- C++
//-*-===//
//
// Copyright 2019 Jean-Michel Gorius
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// =============================================================================
//
// This file declares the types in the stencil dialect.
//
//===----------------------------------------------------------------------===//

#ifndef MLIR_DIALECT_STENCIL_STENCIL_STENCILTYPES_H
#define MLIR_DIALECT_STENCIL_STENCIL_STENCILTYPES_H

#include "mlir/IR/StandardTypes.h"
#include "mlir/IR/TypeSupport.h"
#include "mlir/IR/Types.h"

#include "mlir/Dialect/Stencil/Stencil/StencilEnums.h.inc"

namespace mlir {
namespace stencil {

namespace detail {
struct FieldTypeStorage;
struct VarTypeStorage;
struct PointerTypeStorage;
}

namespace TypeKind {
enum Kind {
  Field = Type::FIRST_STENCIL_TYPE,
  Var,
  Pointer,
  LAST_STENCIL_TYPE = Pointer,
};
}

class FieldType
    : public Type::TypeBase<FieldType, Type, detail::FieldTypeStorage> {
public:
  using Base::Base;

  static bool kindof(unsigned kind) { return kind == TypeKind::Field; }

  // TODO: Handle fields with less or more than 3 dimensions
  static FieldType get(Type elementType);

  Type getElementType() const;
};

class VarType
    : public Type::TypeBase<VarType, Type, detail::VarTypeStorage> {
public:
  using Base::Base;

  static bool kindof(unsigned kind) { return kind == TypeKind::Var; }

  static VarType get(Type contentType);

  Type getContentType() const;
};

class PointerType
    : public Type::TypeBase<PointerType, Type, detail::PointerTypeStorage> {
public:
  using Base::Base;

  static bool kindof(unsigned kind) { return kind == TypeKind::Pointer; }

  static PointerType get(Type pointeeType);

  Type getPointeeType() const;
};

} // namespace stencil
} // namespace mlir

#endif // MLIR_DIALECT_STENCIL_STENCIL_STENCILTYPES_H
