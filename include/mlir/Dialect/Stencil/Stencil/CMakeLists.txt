set(LLVM_TARGET_DEFINITIONS StencilOps.td)
mlir_tablegen(StencilOps.h.inc -gen-op-decls)
mlir_tablegen(StencilOps.cpp.inc -gen-op-defs)
add_public_tablegen_target(MLIRStencilOpsIncGen)

set(LLVM_TARGET_DEFINITIONS StencilBase.td)
mlir_tablegen(StencilEnums.h.inc -gen-enum-decls)
mlir_tablegen(StencilEnums.cpp.inc -gen-enum-defs)
add_public_tablegen_target(MLIRStencilEnumsIncGen)
