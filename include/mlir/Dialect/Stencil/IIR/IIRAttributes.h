//===- IIRAttributes.h - MLIR IIR Attributes --------------------*- C++ -*-===//
//
// Copyright 2019 Jean-Michel Gorius
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// =============================================================================
//
// This file declares the attributes in the IIR dialect.
//
//===----------------------------------------------------------------------===//

#ifndef MLIR_DIALECT_STENCIL_IIR_IIRATTRIBUTES_H
#define MLIR_DIALECT_STENCIL_IIR_IIRATTRIBUTES_H

#include "mlir/Dialect/Stencil/IIR/Cache.h"
#include "mlir/Dialect/Stencil/IIR/Interval.h"
#include "mlir/IR/Attributes.h"

namespace mlir {
namespace iir {

namespace AttrKind {
enum Kind {
  Interval = Attribute::FIRST_IIR_ATTR,
  Cache,
  LAST_IIR_ATTR = Cache,
};
}

namespace detail {
struct IntervalAttributeStorage;
struct CacheAttributeStorage;
} // namespace detail

//===----------------------------------------------------------------------===//
// IntervalAttr
//===----------------------------------------------------------------------===//

class IntervalAttr
    : public Attribute::AttrBase<IntervalAttr, Attribute,
                                 detail::IntervalAttributeStorage> {
public:
  using Base::Base;
  using ValueType = Interval;

  static IntervalAttr get(Interval value, MLIRContext *context);
  static StringRef getAttributeName() { return "interval"; }

  Interval getValue() const;

  /// Methods for support type inquiry through isa, cast, and dyn_cast.
  static bool kindof(unsigned kind) { return kind == AttrKind::Interval; }
};

//===----------------------------------------------------------------------===//
// CacheAttr
//===----------------------------------------------------------------------===//

class CacheAttr : public Attribute::AttrBase<CacheAttr, Attribute,
                                             detail::CacheAttributeStorage> {
public:
  using Base::Base;
  using ValueType = Cache;

  static CacheAttr get(Cache value, MLIRContext *context);
  static StringRef getAttributeName() { return "cache"; }

  Cache getValue() const;

  /// Methods for support type inquiry through isa, cast, and dyn_cast.
  static bool kindof(unsigned kind) { return kind == AttrKind::Cache; }
};

} // namespace iir
} // namespace mlir

#endif // MLIR_DIALECT_STENCIL_IIR_IIRATTRIBUTES_H
