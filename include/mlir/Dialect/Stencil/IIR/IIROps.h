//===- IIROps.h - MLIR IIR operations ---------------------------*- C++ -*-===//
//
// Copyright 2019 Jean-Michel Gorius
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// =============================================================================
//
// This file declares the operations in the stencil IIR dialect.
//
//===----------------------------------------------------------------------===//

#ifndef MLIR_DIALECT_STENCIL_IIR_IIROPS_H
#define MLIR_DIALECT_STENCIL_IIR_IIROPS_H

#include "mlir/Dialect/Stencil/IIR/IIRAttributes.h"
#include "mlir/Dialect/Stencil/IIR/IIRTypes.h"
#include "mlir/IR/Function.h"

namespace mlir {
namespace iir {

#define GET_OP_CLASSES
#include "mlir/Dialect/Stencil/IIR/IIROps.h.inc"

} // namespace iir
} // namespace mlir

#endif // MLIR_DIALECT_STENCIL_IIR_IIROPS_H
