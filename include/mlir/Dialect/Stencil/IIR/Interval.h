#ifndef MLIR_DIALECT_STENCIL_IIR_INTERVAL_H
#define MLIR_DIALECT_STENCIL_IIR_INTERVAL_H

#include <cstdint>

#include "llvm/ADT/DenseMapInfo.h"
#include "llvm/Support/raw_ostream.h"

namespace mlir {
namespace iir {

struct Interval {
  int64_t lowerLevel, lowerOffset;
  int64_t upperLevel, upperOffset;

  static const Interval Invalid;
};

bool operator==(const Interval &lhs, const Interval &rhs);
llvm::raw_ostream &operator<<(llvm::raw_ostream &os, Interval interval);

} // namespace iir
} // namespace mlir

namespace llvm {
template <>
struct DenseMapInfo<mlir::iir::Interval> {
  using T = mlir::iir::Interval;
  using PairInfo = DenseMapInfo<
      std::pair<std::pair<int64_t, int64_t>, std::pair<int64_t, int64_t>>>;

  static T getEmptyKey() {
    auto pair = PairInfo::getEmptyKey();
    return {pair.first.first, pair.first.second, pair.second.first,
            pair.second.second};
  }
  static T getTombstoneKey() {
    auto pair = PairInfo::getTombstoneKey();
    return {pair.first.first, pair.first.second, pair.second.first,
            pair.second.second};
  }
  static unsigned getHashValue(T val) {
    return PairInfo::getHashValue(
        {{val.lowerLevel, val.lowerOffset}, {val.upperLevel, val.upperOffset}});
  }
  static bool isEqual(T lhs, T rhs) { return lhs == rhs; }
};
} // namespace llvm

#endif // MLIR_DIALECT_STENCIL_IIR_INTERVAL_H
