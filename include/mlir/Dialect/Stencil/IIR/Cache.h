#ifndef MLIR_DIALECT_STENCIL_IIR_CACHE_H
#define MLIR_DIALECT_STENCIL_IIR_CACHE_H

#include <cstdint>

#include "mlir/Dialect/Stencil/IIR/IIRTypes.h"
#include "mlir/Dialect/Stencil/IIR/Interval.h"
#include "llvm/ADT/DenseMapInfo.h"
#include "llvm/Support/raw_ostream.h"

namespace mlir {
namespace iir {

struct Window {
  int64_t minus, plus;

  static const Window Invalid;
};

struct Cache {
  int64_t accessID;
  Optional<Window> window;
  iir::CacheType type;
  iir::CachePolicy policy;
  Optional<iir::Interval> interval;
  Optional<iir::Interval> enclosingAccessInterval;
};

bool operator==(const Window &lhs, const Window &rhs);
llvm::raw_ostream &operator<<(llvm::raw_ostream &os, Window window);
bool operator==(const Cache &lhs, const Cache &rhs);
llvm::raw_ostream &operator<<(llvm::raw_ostream &os, Cache cache);

} // namespace iir
} // namespace mlir

namespace llvm {
template <>
struct DenseMapInfo<mlir::iir::Window> {
  using T = mlir::iir::Window;
  using PairInfo = DenseMapInfo<std::pair<int64_t, int64_t>>;

  static T getEmptyKey() {
    auto pair = PairInfo::getEmptyKey();
    return {pair.first, pair.second};
  }
  static T getTombstoneKey() {
    auto pair = PairInfo::getTombstoneKey();
    return {pair.first, pair.second};
  }
  static unsigned getHashValue(T val) {
    return PairInfo::getHashValue({val.minus, val.plus});
  }
  static bool isEqual(T lhs, T rhs) { return lhs == rhs; }
};

// TODO: This is kinda ugly
template <>
struct DenseMapInfo<mlir::iir::Cache> {
  using T = mlir::iir::Cache;
  using PairInfo = DenseMapInfo<std::pair<
      std::pair<int64_t, mlir::iir::Window>,
      std::pair<std::pair<int64_t, int64_t>,
                std::pair<mlir::iir::Interval, mlir::iir::Interval>>>>;

  static T getEmptyKey() {
    auto pair = PairInfo::getEmptyKey();
    return {
        pair.first.first,
        pair.first.second,
        mlir::iir::symbolizeCacheType(pair.second.first.first).getValue(),
        mlir::iir::symbolizeCachePolicy(pair.second.first.second).getValue(),
        pair.second.second.first,
        pair.second.second.second};
  }
  static T getTombstoneKey() {
    auto pair = PairInfo::getTombstoneKey();
    return {
        pair.first.first,
        pair.first.second,
        mlir::iir::symbolizeCacheType(pair.second.first.first).getValue(),
        mlir::iir::symbolizeCachePolicy(pair.second.first.second).getValue(),
        pair.second.second.first,
        pair.second.second.second};
  }
  static unsigned getHashValue(T val) {
    return PairInfo::getHashValue(
        {{val.accessID, val.window.getValueOr(mlir::iir::Window::Invalid)},
         {{static_cast<int64_t>(val.type), static_cast<int64_t>(val.policy)},
          {val.interval.getValueOr(mlir::iir::Interval::Invalid),
           val.enclosingAccessInterval.getValueOr(
               mlir::iir::Interval::Invalid)}}});
  }
  static bool isEqual(T lhs, T rhs) { return lhs == rhs; }
};
} // namespace llvm

#endif // MLIR_DIALECT_STENCIL_IIR_CACHE_H
