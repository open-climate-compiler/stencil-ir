//===- IIRBase.td - MLIR IIR Op Definitions Base file ------*- tablegen -*-===//
//
// Copyright 2019 Jean-Michel Gorius
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// =============================================================================
//
// This is the base file for the stencil IIR operation definition specification.
// This file defines the IIR dialect, common IIR types, and utilities
// for facilitating defining IIR ops.
//
//===----------------------------------------------------------------------===//

#ifndef IIR_BASE
#define IIR_BASE

#ifndef OP_BASE
include "mlir/IR/OpBase.td"
#endif // OP_BASE

//===----------------------------------------------------------------------===//
// IIR dialect definitions
//===----------------------------------------------------------------------===//

def IIR_Dialect : Dialect {
    let name = "iir";

    let description = [{
        The stencil IIR dialect for MLIR.

        This dialect models the concepts developed in the internal immediate
        representation used by the Dawn DSL compiler developed by MeteoSwiss.
    }];

    let cppNamespace = "iir";
}

//===----------------------------------------------------------------------===//
// IIR enum definitions
//===----------------------------------------------------------------------===//

def IIR_MergeDoMethods      : I64EnumAttrCase<"MergeDoMethods", 0>;
def IIR_MergeStages         : I64EnumAttrCase<"MergeStages", 1>;
def IIR_MergeTemporaries    : I64EnumAttrCase<"MergeTemporaries", 2>;
def IIR_NoCodeGen           : I64EnumAttrCase<"NoCodeGen", 3>;
def IIR_UseKCaches          : I64EnumAttrCase<"UseKCaches", 4>;

def IIR_StencilAttr :
    I64EnumAttr<"StencilAttribute", "Stencil attributes", [
        IIR_MergeDoMethods, IIR_MergeStages, IIR_MergeTemporaries,
        IIR_NoCodeGen, IIR_UseKCaches
    ]> {
  let returnType = "::mlir::iir::StencilAttribute";
  let convertFromStorage = "static_cast<::mlir::iir::StencilAttribute>($_self.getInt())";
  let cppNamespace = "::mlir::iir";
}

def IIR_Parallel    : I64EnumAttrCase<"Parallel", 0>;
def IIR_Forward     : I64EnumAttrCase<"Forward", 1>;
def IIR_Backward    : I64EnumAttrCase<"Backward", 2>;

def IIR_LoopOrderAttr :
    I64EnumAttr<"LoopOrder", "Multi stage loop order", [
        IIR_Parallel, IIR_Forward, IIR_Backward
    ]> {
  let returnType = "::mlir::iir::LoopOrder";
  let convertFromStorage = "static_cast<::mlir::iir::LoopOrder>($_self.getInt())";
  let cppNamespace = "::mlir::iir";
}

def IIR_Equal       : I64EnumAttrCase<"Equal", 0>;
def IIR_PlusEqual   : I64EnumAttrCase<"PlusEqual", 1>;
def IIR_MinusEqual  : I64EnumAttrCase<"MinusEqual", 2>;
def IIR_TimesEqual  : I64EnumAttrCase<"TimesEqual", 3>;
def IIR_DivideEqual : I64EnumAttrCase<"DivideEqual", 4>;

def IIR_AssignmentOperatorAttr :
    I64EnumAttr<"AssignmentOperator", "Operator used in an assignment expression", [
        IIR_Equal, IIR_PlusEqual, IIR_MinusEqual,
        IIR_TimesEqual, IIR_DivideEqual
    ]> {
  let returnType = "::mlir::iir::AssignmentOperator";
  let convertFromStorage = "static_cast<::mlir::iir::AssignmentOperator>($_self.getInt())";
  let cppNamespace = "::mlir::iir";
}

def IIR_InvalidType : I64EnumAttrCase<"Invalid", 0>;
def IIR_AutoType    : I64EnumAttrCase<"Auto", 1>;
def IIR_BooleanType : I64EnumAttrCase<"Boolean", 2>;
def IIR_IntegerType : I64EnumAttrCase<"Integer", 3>;
def IIR_FloatType   : I64EnumAttrCase<"Float", 4>;

def IIR_BuiltinTypeAttr :
    I64EnumAttr<"BuiltinType", "Builtin type used for variable declarations", [
        IIR_InvalidType, IIR_AutoType, IIR_BooleanType, IIR_IntegerType,
        IIR_FloatType
    ]> {
  let returnType = "::mlir::iir::BuiltinType";
  let convertFromStorage = "static_cast<::mlir::iir::BuiltinType>($_self.getInt())";
  let cppNamespace = "::mlir::iir";
}

def IIR_Plus    : I64EnumAttrCase<"Plus", 0>;
def IIR_Minus   : I64EnumAttrCase<"Minus", 1>;
def IIR_Times   : I64EnumAttrCase<"Times", 2>;
def IIR_Divide  : I64EnumAttrCase<"Divide", 3>;
def IIR_Lt      : I64EnumAttrCase<"Lt", 4>;
def IIR_Gt      : I64EnumAttrCase<"Gt", 5>;
def IIR_Le      : I64EnumAttrCase<"Le", 6>;
def IIR_Ge      : I64EnumAttrCase<"Ge", 7>;
def IIR_IsEqual : I64EnumAttrCase<"IsEqual", 8>;
def IIR_And     : I64EnumAttrCase<"And", 9>;
def IIR_Or      : I64EnumAttrCase<"Or", 10>;

def IIR_BinaryOperatorAttr :
    I64EnumAttr<"BinaryOperator", "Binary operator type", [
        IIR_Plus, IIR_Minus, IIR_Times, IIR_Divide,
        IIR_Lt, IIR_Gt, IIR_Le, IIR_Ge, IIR_IsEqual,
        IIR_And, IIR_Or
    ]> {
  let returnType = "::mlir::iir::BinaryOperator";
  let convertFromStorage = "static_cast<::mlir::iir::BinaryOperator>($_self.getInt())";
  let cppNamespace = "::mlir::iir";
}

def IIR_UnaryMinus   : I64EnumAttrCase<"UnaryMinus", 0>;

def IIR_UnaryOperatorAttr :
    I64EnumAttr<"UnaryOperator", "Unary operator type", [
        IIR_UnaryMinus
    ]> {
  let returnType = "::mlir::iir::UnaryOperator";
  let convertFromStorage = "static_cast<::mlir::iir::UnaryOperator>($_self.getInt())";
  let cppNamespace = "::mlir::iir";
}

def IIR_IJ      : I64EnumAttrCase<"IJ", 0>;
def IIR_K       : I64EnumAttrCase<"K", 1>;
def IIR_IJK     : I64EnumAttrCase<"IJK", 2>;
def IIR_Bypass  : I64EnumAttrCase<"Bypass", 3>;

def IIR_CacheTypeAttr :
    I64EnumAttr<"CacheType", "Multi-stage cache type", [
        IIR_IJ, IIR_K, IIR_IJK, IIR_Bypass
    ]> {
  let returnType = "::mlir::iir::CacheType";
  let convertFromStorage = "static_cast<::mlir::iir::CacheType>($_self.getInt())";
  let cppNamespace = "::mlir::iir";
}

def IIR_Unknown     : I64EnumAttrCase<"Unknown", 0>;
def IIR_FillFlush   : I64EnumAttrCase<"FillFlush", 1>;
def IIR_Fill        : I64EnumAttrCase<"Fill", 2>;
def IIR_Flush       : I64EnumAttrCase<"Flush", 3>;
def IIR_EPFlush     : I64EnumAttrCase<"EPFlush", 4>;
def IIR_BPFill      : I64EnumAttrCase<"BPFill", 5>;
def IIR_Local       : I64EnumAttrCase<"Local", 6>;

def IIR_CachePolicyAttr :
    I64EnumAttr<"CachePolicy", "Multi-stage cache policy", [
        IIR_Unknown, IIR_FillFlush, IIR_Fill, IIR_Flush, IIR_EPFlush,
        IIR_BPFill, IIR_Local
    ]> {
  let returnType = "::mlir::iir::CachePolicy";
  let convertFromStorage = "static_cast<::mlir::iir::CachePolicy>($_self.getInt())";
  let cppNamespace = "::mlir::iir";
}

def IIR_DimI        : I64EnumAttrCase<"I", 0>;
def IIR_DimJ        : I64EnumAttrCase<"J", 1>;
def IIR_DimK        : I64EnumAttrCase<"K", 2>;
def IIR_DimInvalid  : I64EnumAttrCase<"Invalid", 3>;

def IIR_DimensionAttr :
    I64EnumAttr<"Dimension", "Dimension", [
        IIR_DimI, IIR_DimJ, IIR_DimK, IIR_DimInvalid
    ]> {
  let returnType = "::mlir::iir::Dimension";
  let convertFromStorage = "static_cast<::mlir::iir::Dimension>($_self.getInt())";
  let cppNamespace = "::mlir::iir";
}

def IIR_GlobalBooleanType : I64EnumAttrCase<"Boolean", 0>;
def IIR_GlobalIntegerType : I64EnumAttrCase<"Integer", 1>;
def IIR_GlobalDoubleType  : I64EnumAttrCase<"Double", 2>;

def IIR_GlobalTypeAttr :
    I64EnumAttr<"GlobalType", "Global variable type", [
        IIR_GlobalBooleanType, IIR_GlobalIntegerType, IIR_GlobalDoubleType
    ]> {
  let returnType = "::mlir::iir::Dimension";
  let convertFromStorage = "static_cast<::mlir::iir::Dimension>($_self.getInt())";
  let cppNamespace = "::mlir::iir";
}

//===----------------------------------------------------------------------===//
// IIR OpTrait definitions
//===----------------------------------------------------------------------===//

class IIR_ScopeVerifier<string opName> : PredOpTrait<
  "op must appear in a '" # opName #"' block",
  CPred<"llvm::isa_and_nonnull<iir::" # opName # ">($_op.getParentOp())">>;

def InIIRScope : IIR_ScopeVerifier<"IIROp">;
def InStencilScope : IIR_ScopeVerifier<"StencilOp">;
def InMultiStageScope : IIR_ScopeVerifier<"MultiStageOp">;
def InStageScope : IIR_ScopeVerifier<"StageOp">;
def InDoMethodScope : IIR_ScopeVerifier<"DoMethodOp">;
def InStatementAccessPairScope : IIR_ScopeVerifier<"StatementAccessPairOp">;
def InASTStatementScope : IIR_ScopeVerifier<"ASTStatementOp">;
def InExprStatementScope : IIR_ScopeVerifier<"ExprStatementOp">;
def InAssignmentExprScope : IIR_ScopeVerifier<"AssignmentExprOp">;
def InLeftScope : IIR_ScopeVerifier<"LeftOp">;
def InRightScope : IIR_ScopeVerifier<"RightOp">;
def InFieldAccessExprScope : IIR_ScopeVerifier<"FieldAccessExprOp">;
def InVarDeclStatementScope : IIR_ScopeVerifier<"VarDeclStatementOp">;
def InBinaryOperatorScope : IIR_ScopeVerifier<"BinaryOperatorOp">;
def InTernaryOperatorScope : IIR_ScopeVerifier<"TernaryOperatorOp">;
def InCondScope : IIR_ScopeVerifier<"CondOp">;
def InUnaryOperatorScope : IIR_ScopeVerifier<"UnaryOperatorOp">;
def InFunCallScope : IIR_ScopeVerifier<"FunCallOp">;
def InStencilFunCallScope : IIR_ScopeVerifier<"StencilFunCallOp">;
def InIfScope : IIR_ScopeVerifier<"IfOp">;
def InThenScope : IIR_ScopeVerifier<"ThenOp">;
def InElseScope : IIR_ScopeVerifier<"ElseOp">;
def InBlockStatementScope : IIR_ScopeVerifier<"BlockStatementOp">;
def InVarAccessExprScope : IIR_ScopeVerifier<"VarAccessExprOp">;
def InIndexScope : IIR_ScopeVerifier<"IndexOp">;
def InIDToStencilCallEntryScope : IIR_ScopeVerifier<"IDToStencilCallEntryOp">;
def InStencilCallDeclStatementScope : IIR_ScopeVerifier<"StencilCallDeclStatementOp">;

//===----------------------------------------------------------------------===//
// IIR attribute definitions
//===----------------------------------------------------------------------===//

// Base class for interval attributes.
class IntervalAttrBase<Pred condition, string description> :
    Attr<condition, description> {
  let storageType = [{ iir::IntervalAttr }];
  let returnType = [{ iir::IntervalAttr }];
  let convertFromStorage = "$_self";
}
def IntervalAttr : IntervalAttrBase<CPred<"$_self.isa<iir::IntervalAttr>()">,
                                    "interval attribute">;

// Base class for cache attributes.
class CacheAttrBase<Pred condition, string description> :
    Attr<condition, description> {
  let storageType = [{ iir::CacheAttr }];
  let returnType = [{ iir::CacheAttr }];
  let convertFromStorage = "$_self";
}
def CacheAttr : CacheAttrBase<CPred<"$_self.isa<iir::CacheAttr>()">,
                                    "cache attribute">;

//===----------------------------------------------------------------------===//
// IIR op definitions
//===----------------------------------------------------------------------===//

// Base class for all IIR ops.
class IIR_Op<string mnemonic, list<OpTrait> traits = []> :
    Op<IIR_Dialect, mnemonic, traits> {

    // For each IIR op, the following static functions need to be defined in
    // IIROps.cpp:
    //
    // * static ParseResult parse<op-c++-class-name>(OpAsmParser &parser,
    //                                               OperationState &state);
    // * static void print(OpAsmPrinter &p, <op-c++-class-name> op)
    // * static LogicalResult verify(<op-c++-class-name> op)
    let parser = [{ return ::parse$cppClass(parser, result); }];
    let printer = [{ ::print(*this, p); }];
    let verifier = [{ return ::verify(*this); }];
}

#endif // IIR_BASE
