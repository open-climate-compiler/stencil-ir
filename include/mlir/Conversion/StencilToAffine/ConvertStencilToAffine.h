//===- ConvertStencilToAffine.h - Convert Stencil to Affine ops -*- C++ -*-===//
//
// Copyright 2019 Fabian Wolff
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// =============================================================================
//
// Provides patterns to convert from Stencil structure ops to affine ops.
//
//===----------------------------------------------------------------------===//

#ifndef MLIR_CONVERSION_STENCILTOAFFINE_CONVERTSTENCILTOAFFINE_H
#define MLIR_CONVERSION_STENCILTOAFFINE_CONVERTSTENCILTOAFFINE_H

#include "mlir/Dialect/Stencil/Stencil/StencilOps.h"
#include "mlir/Support/StringExtras.h"
#include "mlir/Transforms/DialectConversion.h"

namespace mlir {

/// Collect a set of patterns to lower from Stencil structure
/// operations (stencil.stage, stencil.do_method etc.) to loop
/// operations within the Affine dialect; in particular, convert
/// abstract stencil descriptions into affine loop nests.
void populateStencilToAffineConversionPatterns(OwningRewritePatternList &patterns,
                                     MLIRContext *ctx);

void populateStencilToAffineConversionPatterns_2(OwningRewritePatternList &patterns,
                                       MLIRContext *ctx);

} // namespace mlir

#endif // MLIR_CONVERSION_STENCILTOAFFINE_CONVERTSTENCILTOAFFINE_H
